﻿using System.Windows.Controls;
using System.Windows.Input;

namespace WMOEditor
{
    public class Gui : Utils.Singleton<Gui, Utils.DemandPolicy<Gui>>
    {
        public void SetGraphicsWindow(MainWindow wnd)
        {
            mWindow = wnd;

            mGroupList = mWindow.GroupListBox;

            wnd.RenderControl.MouseMove += UpdateMousePosition;
        }

        void UpdateMousePosition(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            var parameters = new Model.ModelPickParameters()
            {
                PickPosition = new System.Drawing.Point(e.X, e.Y)
            };

            Model.ModelPicker.PickModel(parameters);
            /*if (parameters.HasHit)
                mWindow.HitPointStatusLabel.Content = "Hit point: " + parameters.HitPoint + " Model: " + parameters.InstanceHit.Model.ModelName;
            else
                mWindow.HitPointStatusLabel.Content = "Hit poit: no hit";*/
        }

        public void SetModel(Model.WmoRoot model)
        {
            mWmoRoot = model;

            mGroupList.Items.Clear();

            foreach (var group in mWmoRoot.Groups)
            {
                mGroupList.Items.Add(new UI.GroupListEntry(group)
                {
                    Title = group.ToString(),
                }
                );
            }

            mWindow.DoodadListBox.Items.Clear();

            foreach (var doodad in mWmoRoot.GetDoodadNames())
            {
                mWindow.DoodadListBox.Items.Add(doodad);
            }

            mWindow.DoodadSetBox.Items.Clear();

            var index = 0;
            foreach (var set in mWmoRoot.DoodadSets)
            {
                var setName = set.GetSetName();
                if(string.IsNullOrWhiteSpace(setName))
                    setName = "set_" + index;

                var setEntry = new UI.DoodadSetEntry(index == 0, setName, (int)set.numDoodads, index);
                ++index;

                mWindow.DoodadSetBox.Items.Add(setEntry);
            }
        }

        public void GuiCommand(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == GroupRender)
            {
                var group = e.Parameter as Model.WmoGroup;

                var cb = e.OriginalSource as CheckBox;

                if (group != null && cb != null)
                {
                    if (cb.IsChecked != null) @group.RenderEnabled = cb.IsChecked.Value;
                }
            }
            else if (e.Command == SetChange)
            {
                var rb = e.OriginalSource as RadioButton;
                if (rb?.IsChecked != null && rb.IsChecked.Value)
                    mWmoRoot.ChangeSetIndex((int)e.Parameter);
            }
            else if (e.Command == EditGroup)
            {
                var group = e.Parameter as Model.WmoGroup;
                var ge = new UI.GroupEditor();
                ge.LoadFromGroup(group);
                ge.ShowDialog();
            }
        }

        public static readonly RoutedCommand GroupRender = new RoutedCommand();
        public static readonly RoutedCommand SetChange = new RoutedCommand();
        public static readonly RoutedCommand EditGroup = new RoutedCommand();

        private ListBox mGroupList;
        private MainWindow mWindow;
        private Model.WmoRoot mWmoRoot;
    }
}
