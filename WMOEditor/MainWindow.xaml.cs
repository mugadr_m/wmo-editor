﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace WMOEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            mRenderControl = new Rendering.RenderControl();

            renderHost.Child = mRenderControl;

            Gui.Instance.SetGraphicsWindow(this);

            Rendering.RenderLoop.Instance.Init(this, renderHost.Child);

            LoadWmoModels();
        }

        private void renderHost_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            Rendering.RenderLoop.Instance.OnSizeChanged(renderHost.Child.ClientSize);
        }

        public Rendering.RenderControl RenderControl { get { return mRenderControl; } }
        public ListBox GroupListBox { get { return groupListBox; } }
        public ListBox DoodadListBox { get { return doodadBox; } }
        public ListBox DoodadSetBox { get { return doodadSetList; } }
        public StatusBarItem HitPointStatusLabel { get { return hitPointStatusLabel; } }

        private Rendering.RenderControl mRenderControl;

        private void GuiCommand(object sender, ExecutedRoutedEventArgs e)
        {
            Gui.Instance.GuiCommand(sender, e);
        }

        class TreeViewItemEntry
        {
            public TreeViewItem Item;
            public Dictionary<string, TreeViewItemEntry> SubItems = new Dictionary<string, TreeViewItemEntry>();
        }

        private void LoadWmoModels()
        {
            var rootLevel = new Dictionary<string, TreeViewItemEntry>();

            foreach (var archive in Mpq.FileManager.Instance.ArchiveList.Values)
            {
                foreach (var file in archive.GetFiles())
                {
                    var name = file.ToLower();
                    if (name.EndsWith(".wmo") == false)
                        continue;

                    var fileEnd = name.Substring(name.Length - 7, 3);
                    uint index = 0;
                    if (uint.TryParse(fileEnd, out index))
                        continue;

                    var parts = name.Split('\\');
                    if (parts.Length == 0)
                        continue;

                    TreeViewItemEntry rootItem = null;

                    if (rootLevel.ContainsKey(parts[0]))
                        rootItem = rootLevel[parts[0]];
                    else
                    {
                        rootItem = new TreeViewItemEntry();
                        rootItem.Item = new TreeViewItem();
                        rootItem.Item.Header = parts[0];
                        rootLevel.Add(parts[0], rootItem);
                    }

                    for (var i = 1; i < parts.Length; ++i)
                    {
                        var part = parts[i];

                        if (rootItem.SubItems.ContainsKey(part))
                        {
                            rootItem = rootItem.SubItems[part];
                            continue;
                        }

                        var nextItem = new TreeViewItem();
                        nextItem.Header = part;

                        var tvie = new TreeViewItemEntry();
                        tvie.Item = nextItem;

                        rootItem.SubItems.Add(part, tvie);
                        rootItem.Item.Items.Add(nextItem);
                        rootItem = tvie;
                    }
                }
            }

            foreach (var item in rootLevel)
                wmoListTree.Items.Add(item.Value.Item);
        }

        private void wmoListTree_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
        {
            var selItem = wmoListTree.SelectedItem;
            if (selItem == null)
                return;

            var itm = selItem as TreeViewItem;
            var wmo = GetFullPath(itm);

            Rendering.RenderLoop.Instance.UpdateModel(wmo);
        }

        private string GetFullPath(TreeViewItem node)
        {
            if (node == null)
                throw new ArgumentNullException();

            var result = Convert.ToString(node.Header);

            for (var i = GetParent(node); i != null; i = GetParent(i))
                result = i.Header + "\\" + result;

            return result;
        }

        private TreeViewItem GetParent(TreeViewItem item)
        {
            var prop = item.GetType().GetProperty("ParentTreeViewItem", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

            return (TreeViewItem)prop.GetValue(item);
        }
    }
}
