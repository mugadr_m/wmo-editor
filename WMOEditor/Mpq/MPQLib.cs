﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace WMOEditor.Mpq
{
    internal class FileManager : Utils.Singleton<FileManager, Utils.DemandPolicy<FileManager>>
    {
        #region Imported Functions

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static extern byte SFileOpenArchive(string archiveName, uint priority, uint flags, ref IntPtr hArchive);

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        internal static extern byte SFileOpenFileEx(IntPtr hArchive, string fileName, uint searchScope, ref IntPtr hFile);

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        internal static extern uint SFileGetFileSize(IntPtr hFile, ref uint fileSizeHigh);

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        internal static extern byte SFileReadFile(IntPtr hFile, [In, Out] byte[] lpBuffer, int numBytes, ref int bytesRead, IntPtr lpOverlapped);

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall)]
        internal static extern byte SFileCloseFile(IntPtr hArchive);

        [DllImport("Stormlib.dll", CallingConvention = CallingConvention.StdCall)]
        internal static extern byte SFileHasFile(IntPtr hArchive, string file);

        #endregion

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum Locales
        {
            enGB,
            deDE,
            frFR,
            enUS,
            esES,
            esMX,
            ruRU,
            chCH,
            Unknown
        }

        public event Action Initialized;

        public bool Init(string gamePath)
        {
            ArchiveList = new Dictionary<string, Archive>();

            var basePath = gamePath;
            basePath += "\\data";
            LoadArchivesFromDir(basePath);

            /*var locs = Enum.GetNames(typeof(Locales));
            foreach (var loc in locs)
            {
                if (System.IO.Directory.Exists(basePath + "\\" + loc))
                {
                    Locale = (Locales)Enum.Parse(typeof(Locales), loc);
                    break;
                }
            }

            if (Locale == Locales.Unknown)
                throw new Exception("Unable to determine locale!");

            basePath += "\\" + Locale.ToString();
            LoadArchivesFromDir(basePath);*/

            Initialized?.Invoke();

            return true;
        }

        private void LoadArchive(List<string> listFiles)
        {
            foreach (var file in listFiles)
            {
                var hArchive = new IntPtr(0);
                var ret = SFileOpenArchive(file, 0, 0x100, ref hArchive);
                if (ret == 0) continue;
                Archives.Add(file, hArchive);
                ArchiveList.Add(file, new Archive(hArchive));
            }
        }

        private void LoadArchivesFromDir(string dir)
        {
            var files = Directory.GetFiles(dir, "*.mpq");
            var listFiles = files.ToList();
            SortLambda(listFiles);
            LoadArchive(listFiles);
        }

        private static void SortLambda(List<string> listFiles)
        {
            listFiles.Sort(
                (strA, strB) =>
                {
                    if (strA == null && strB == null)
                        return 0;
                    if (strA == null)
                        return 1;
                    if (strB == null)
                        return -1;

                    var patchIndexA = strA.ToLower().IndexOf("patch", StringComparison.Ordinal);
                    var patchIndexB = strB.ToLower().IndexOf("patch", StringComparison.Ordinal);

                    if (patchIndexA != -1 && patchIndexB == -1)
                        return -1;
                    if (patchIndexB != -1 && patchIndexA == -1)
                        return 1;

                    if (patchIndexA == -1 && patchIndexB == -1)
                        return string.Compare(strA, strB, StringComparison.Ordinal);

                    var extIndexA = strA.LastIndexOf('.');
                    var extIndexB = strB.LastIndexOf('.');

                    var patchIdentA = strA.Substring(extIndexA - 1, 1)[0];
                    var patchIdentB = strB.Substring(extIndexB - 1, 1)[0];

                    var separatorA = strA.Substring(extIndexA - 2, 1)[0];
                    var separatorB = strB.Substring(extIndexB - 2, 1)[0];

                    if (separatorA != '-' && separatorB == '-')
                        return 1;
                    if (separatorA == '-' && separatorB != '-')
                        return -1;

                    if (patchIdentA == patchIdentB)
                        return 0;
                    if (patchIdentA < patchIdentB)
                        return 1;
                    if (patchIdentB < patchIdentA)
                        return -1;

                    return 0;
                }
            );
        }

        internal Dictionary<string, IntPtr> Archives = new Dictionary<string, IntPtr>();

        public Dictionary<string, Archive> ArchiveList { get; private set; }
    }

    public class Archive
    {
        public Archive(IntPtr handle)
        {
            mHandle = handle;
        }

        public string[] GetFiles()
        {
            var lf = IntPtr.Zero;
            var ret = FileManager.SFileOpenFileEx(mHandle, "(listfile)", 0, ref lf);
            if (ret == 0)
                return new string[] { };

            var file = new File();
            file.Load(lf);
            var bytes = file.Read(file.FileSize);
            var fullStr = Encoding.UTF8.GetString(bytes);
            return fullStr.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        }

        private readonly IntPtr mHandle;
    }

    public class File : Stream
    {
        private byte[] fileData;
        private readonly IntPtr fileHandle = IntPtr.Zero;

        public static bool Exists(string fileName) {
            return FileManager.Instance.Archives.Any(val => FileManager.SFileHasFile(val.Value, fileName) != 0);
        }

        public File(string fileName)
        {
            lock (mAccessLock)
            {
                FileName = fileName;
                foreach (var hArchive in FileManager.Instance.Archives)
                {
                    var ret = FileManager.SFileOpenFileEx(hArchive.Value, fileName, 0, ref fileHandle);
                    if (ret != 0)
                        break;
                    fileHandle = IntPtr.Zero;
                }

                if (fileHandle == IntPtr.Zero)
                    throw new FileNotFoundException("No MPQ-Archive contains the file!", fileName);

                Load(fileHandle);
            }
        }

        internal File()
        {
        }

        internal void Load(IntPtr hFile)
        {
            uint sizeHigh = 0;
            var fileSize = FileManager.SFileGetFileSize(hFile, ref sizeHigh);

            if (fileSize == 0)
                throw new FileNotFoundException("No MPQ-Archive contains the file with valid size!", FileName);

            fileData = new byte[fileSize];
            var bytesRead = 0;

            FileSize = fileSize;
            Position = 0;
            FileManager.SFileReadFile(hFile, fileData, (int)fileSize, ref bytesRead, IntPtr.Zero);
            FileManager.SFileCloseFile(hFile);
        }

        public uint FileSize { get; private set; }
        public override long Position { get; set; }
        public string FileName { get; private set; }

        public GCHandle GetPointer()
        {
            var handle = GCHandle.Alloc(fileData, GCHandleType.Pinned);
            return handle;
        }

        public void Read<T>(ref T input) where T : struct
        {
            var size = Marshal.SizeOf(input);
            if (Position + size > FileSize)
                throw new ArgumentOutOfRangeException();

            var ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(fileData, (int)Position, ptr, size);
            input = (T)Marshal.PtrToStructure(ptr, typeof(T));
            Marshal.FreeHGlobal(ptr);
            Position += (uint)size;
        }

        public void Read<T>(T[] arr) where T : struct
        {
            var hdl = GCHandle.Alloc(arr, GCHandleType.Pinned);
            var size = Marshal.SizeOf(typeof(T)) * arr.Length;
            var bt = Read((uint)size);
            Marshal.Copy(bt, 0, hdl.AddrOfPinnedObject(), size);
            hdl.Free();
        }

        public T ReadAt<T>(uint offset) where T : struct
        {
            var oldPos = Position;
            Position = offset;
            var ret = Read<T>();
            Position = oldPos;
            return ret;
        }

        public T Read<T>() where T : struct
        {
            var ret = new T();
            Read(ref ret);
            return ret;
        }

        public byte[] Read(uint numBytes)
        {
            if (Position + numBytes > FileSize)
                throw new ArgumentOutOfRangeException();

            var ret = new byte[numBytes];
            Array.Copy(fileData, (int)Position, ret, 0, (int)numBytes);
            Position += numBytes;
            return ret;
        }

        public uint ReadUInt()
        {
            if (Position + 4 > FileSize)
                throw new ArgumentOutOfRangeException();
            Position += 4;
            return BitConverter.ToUInt32(fileData, (int)Position - 4);
        }

        public short ReadShort()
        {
            if (Position + 2 > FileSize)
                throw new ArgumentOutOfRangeException();
            Position += 2;
            return BitConverter.ToInt16(fileData, (int)Position - 2);
        }

        public float ReadFloat()
        {
            if (Position + 4 > FileSize)
                throw new ArgumentOutOfRangeException();
            Position += 4;
            return BitConverter.ToSingle(fileData, (int)Position - 4);
        }

        public static File FromHandle(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
                return null;

            var ret = new File();
            ret.Load(handle);
            ret.FileName = "";
            return ret;
        }

        public override bool CanRead { get { return (Length - Position) > 0; } }
        public override bool CanSeek { get { return true; } }
        public override bool CanWrite { get { return false; } }
        public override void Close()
        {
        }
        public override long Length
        {
            get { return FileSize; }
        }
        public override void Flush()
        {
            throw new NotImplementedException();
        }
        public override long Seek(long offset, SeekOrigin origin)
        {
            switch (origin)
            {
                case SeekOrigin.Begin:
                    Position = offset;
                    break;

                case SeekOrigin.Current:
                    Position += offset;
                    break;

                case SeekOrigin.End:
                    Position = Length - offset;
                    break;
            }

            return Position;
        }
        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }
        public override int Read(byte[] buffer, int offset, int count)
        {
            var bytes = Read((uint)count);
            Buffer.BlockCopy(bytes, 0, buffer, offset, count);
            return bytes.Length;
        }
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        private static object mAccessLock = new object();
    }
}
