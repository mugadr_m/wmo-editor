﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using WMOEditor.Utils;

namespace WMOEditor.Rendering
{
    public class RenderLoop : Singleton<RenderLoop, DemandPolicy<RenderLoop>>
    {
        /// <summary>
        /// Loads the graphics device based on the renderCtrl and its size and attaches
        /// the render loop to the window.
        /// </summary>
        /// <param name="dstWindow">The window in which the render loop will run</param>
        /// <param name="renderCtrl">The control acting as target for the rendering operations</param>
        public void Init(Window dstWindow, Control renderCtrl)
        {
            mDevice = new GxDevice(renderCtrl.Handle, renderCtrl.ClientSize);

            mRenderTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(10), DispatcherPriority.ApplicationIdle, OnIdle, dstWindow.Dispatcher);
            mRenderTimer.Start();

            mModel = new Model.WmoRoot(@"world\wmo\kalimdor\buildings\orctower\orctower.wmo", new Mpq.File(@"world\wmo\kalimdor\buildings\orctower\orctower.wmo"));
            mModel.Load();

            Gui.Instance.SetModel(mModel);

            mDevice.OnFrame += d => mModel.Render();
        }

        /// <summary>
        /// Informs the renderer to load a new model and display it.
        /// </summary>
        /// <param name="modelName">Name of the model to display</param>
        public void UpdateModel(string modelName)
        {
            if (Mpq.File.Exists(modelName) == false)
                return;

            mModel.Unload();

            mModel = new Model.WmoRoot(modelName, new Mpq.File(modelName));
            mModel.Load();

            Gui.Instance.SetModel(mModel);
        }

        /// <summary>
        /// Informs the graphics device that the size of the render area has changed and the
        /// backbuffer whould be resized.
        /// </summary>
        /// <param name="size">New size of the rendering area</param>
        public void OnSizeChanged(System.Drawing.Size size)
        {
            mDevice.Resize(size);
        }

        /// <summary>
        /// Callback for the render dispatcher timer.
        /// </summary>
        /// <param name="sender">unused</param>
        /// <param name="args">unused</param>
        private void OnIdle(object sender, EventArgs args)
        {
            mDevice.RenderFrame();
            if (OnFrame != null)
                OnFrame();
        }

        /// <summary>
        /// Gets the graphics device attached to the loop.
        /// </summary>
        public GxDevice Device { get { return mDevice; } }

        public event Action OnFrame;

        private DispatcherTimer mRenderTimer;
        private GxDevice mDevice;

        private Model.WmoRoot mModel;
    }
}
