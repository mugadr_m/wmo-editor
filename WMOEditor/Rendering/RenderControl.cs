﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WMOEditor.Rendering
{
    public partial class RenderControl : UserControl
    {
        public RenderControl()
        {
            InitializeComponent();
        }

        private void RenderControl_Load(object sender, EventArgs e)
        {
            BackColor = Color.CornflowerBlue;
        }
    }
}
