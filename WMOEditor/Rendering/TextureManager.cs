﻿using System;
using System.Collections.Generic;

namespace WMOEditor.Rendering
{
    public class TextureManager : Utils.Singleton<TextureManager, Utils.DemandPolicy<TextureManager>>
    {
        /// <summary>
        /// Holds weak references associated by their file names hash.
        /// </summary>
        private Dictionary<int, WeakReference<Texture>> mTextureCache = new Dictionary<int, WeakReference<Texture>>();

        /// <summary>
        /// Attempts to retreive a texture by its file name which is not case sensitive.
        /// </summary>
        /// <param name="name">Case insensitive file path in the MPQ</param>
        /// <returns>A texture object with the blp texture loaded</returns>
        public Texture GetTexture(string name)
        {
            var hash = name.ToLower().GetHashCode();

            if (mTextureCache.ContainsKey(hash))
            {
                var reference = mTextureCache[hash];

                Texture tex;
                if (reference.TryGetTarget(out tex))
                    return tex;
            }

            var ret = new Texture(new Mpq.File(name));
            if (mTextureCache.ContainsKey(hash))
            {
                mTextureCache[hash].SetTarget(ret);
            }
            else
            {
                mTextureCache.Add(hash, new WeakReference<Texture>(ret));
            }

            return ret;
        }

        public static Texture DefaultTexture;

        static TextureManager()
        {
            DefaultTexture = new Texture(new Mpq.File(@"World\Scale\1_NULL.blp"));
        }
    }
}
