﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.Direct3D9;

namespace WMOEditor.Rendering
{
    public class ShaderProgram
    {
        public ShaderProgram(ShaderCode vertex, ShaderCode pixel)
        {
            mVertexCode = vertex;
            mPixelCode = pixel;

            mVertexShader = mVertexCode.CreateVertexShader();
            mPixelShader = mPixelCode.CreatePixelShader();
        }

        public void Set<T>(string name, T value) where T : struct
        {
            var handle = mVertexCode.ConstantTable.GetConstant(null, name);
            if (handle == null)
                handle = mPixelCode.ConstantTable.GetConstant(null, name);

            if (handle == null)
                throw new InvalidOperationException("Shader variable not found");

            mVertexCode.ConstantTable.SetValue(Rendering.RenderLoop.Instance.Device.Device, handle, value);
        }

        public void Apply()
        {
            var dev = Rendering.RenderLoop.Instance.Device.Device;
            dev.VertexShader = mVertexShader;
            dev.PixelShader = mPixelShader;
        }

        private VertexShader mVertexShader;
        private PixelShader mPixelShader;
        private ShaderCode mVertexCode;
        private ShaderCode mPixelCode;
    }
}
