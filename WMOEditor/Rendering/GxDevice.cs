﻿using System;
using System.Drawing;
using SlimDX.Direct3D9;
using SlimDX;

namespace WMOEditor.Rendering
{
    public class GxDevice
    {
        /// <summary>
        /// Initializes the Direct3D interface based on the parameters for this method.
        /// </summary>
        /// <param name="wndHandle">Native handle of the destination window for rendering</param>
        /// <param name="renderSize">Initial size of the back buffer</param>
        public GxDevice(IntPtr wndHandle, Size renderSize)
        {
            mDriver = new Direct3D();

            mParams = new PresentParameters()
            {
                AutoDepthStencilFormat = Format.D16,
                BackBufferCount = 1,
                BackBufferFormat = Format.A8R8G8B8,
                BackBufferHeight = renderSize.Height,
                BackBufferWidth = renderSize.Width,
                DeviceWindowHandle = wndHandle,
                EnableAutoDepthStencil = true,
                FullScreenRefreshRateInHertz = 0,
                Multisample = MultisampleType.None,
                MultisampleQuality = 0,
                PresentationInterval = PresentInterval.One,
                PresentFlags = PresentFlags.None,
                SwapEffect = SwapEffect.Discard,
                Windowed = true
            };

            FillDepthBufferFormat();
            FillMultiSample();

            mDevice = new Device(mDriver, 0, DeviceType.Hardware, wndHandle, CreateFlags.HardwareVertexProcessing, mParams);

            Camera = new Camera();

            ProjectionMatrix = Matrix.PerspectiveFovLH(45.0f * 3.141592f / 180.0f, renderSize.Width / (float)renderSize.Height, 0.3f, 1000.0f);

            mDevice.SetTransform(TransformState.Projection, ProjectionMatrix);
            mDevice.SetTransform(TransformState.View, Camera.ViewMatrix);
            mDevice.SetTransform(TransformState.World, Matrix.Identity);

            mDevice.SetRenderState(RenderState.Lighting, false);
            mDevice.SetRenderState(RenderState.ZEnable, true);
            mDevice.SetRenderState(RenderState.CullMode, Cull.None);
        }

        /// <summary>
        /// Resizes the back buffer to the new size.
        /// </summary>
        /// <param name="newSize">New size of the back buffer</param>
        public void Resize(Size newSize)
        {
            mParams.BackBufferHeight = newSize.Height;
            mParams.BackBufferWidth = newSize.Width;

            OnLostDevice();
            mDevice.Reset(mParams);
            OnResetDevice();
        }

        /// <summary>
        /// Performs one frame drawing.
        /// </summary>
        public void RenderFrame()
        {
            Camera.Update();
            mDevice.SetTransform(TransformState.View, Camera.ViewMatrix);

            mDevice.Clear(ClearFlags.Target | ClearFlags.ZBuffer, Color.Cornsilk, 1.0f, 0);
            mDevice.BeginScene();

            if (OnFrame != null)
                OnFrame(Device);

            mDevice.EndScene();

            try
            {
                mDevice.Present();
            }
            catch (SlimDXException e)
            {
                if (e.HResult == -2146233088)
                {
                    while ((uint)mDevice.TestCooperativeLevel().Code != 0x88760869)
                    {
                        System.Threading.Thread.Sleep(10);
                    }

                    OnLostDevice();
                    mDevice.Reset(mParams);
                    OnResetDevice();
                    return;
                }

                throw;
            }
        }

        /// <summary>
        /// Called when the device lost and about to be reset. Should free all
        /// resources not in managed pool.
        /// </summary>
        private void OnLostDevice()
        {
        }

        /// <summary>
        /// Called right after the device has been reset. Should renew all the
        /// resources not in managed pool.
        /// </summary>
        private void OnResetDevice()
        {
            ProjectionMatrix = Matrix.PerspectiveFovLH(45.0f * 3.141592f / 180.0f, mParams.BackBufferWidth / (float)mParams.BackBufferHeight, 0.3f, 1000.0f);

            mDevice.SetTransform(TransformState.Projection, ProjectionMatrix);
            mDevice.SetTransform(TransformState.World, Matrix.Identity);

            mDevice.SetRenderState(RenderState.Lighting, false);
            mDevice.SetRenderState(RenderState.ZEnable, true);
            mDevice.SetRenderState(RenderState.CullMode, Cull.None);

            mDevice.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            mDevice.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            mDevice.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
        }

        /// <summary>
        /// Checks the highest available depth buffer format for the default graphics
        /// adapter and fills the mParams structures AutoDepthStencilFormat member.
        /// </summary>
        private void FillDepthBufferFormat()
        {
            var testFormats = new[]
            {
                Format.D16,
                Format.D24X8,
                Format.D24S8,
            };

            for (var i = 2; i >= 0; --i)
            {
                if (mDriver.CheckDepthStencilMatch(0, DeviceType.Hardware, Format.A8R8G8B8, Format.A8R8G8B8, testFormats[i]))
                {
                    mParams.AutoDepthStencilFormat = testFormats[i];
                    return;
                }
            }

            throw new Exception("Could not find a depth format supported by the graphics card.");
        }

        /// <summary>
        /// Checks for the highest available multisample type and quality for
        /// the default graphics adapter and sets the mParams.Multisample
        /// and mParams.MultisampleQuality members to that value.
        /// </summary>
        private void FillMultiSample()
        {
            var curType = MultisampleType.TwoSamples;

            var maxType = MultisampleType.None;
            var quality = 0;

            for (; curType <= MultisampleType.SixteenSamples; ++curType)
            {
                int numLevels;

                if (mDriver.CheckDeviceMultisampleType(0, DeviceType.Hardware, Format.A8R8G8B8, true, curType, out numLevels) && numLevels > 0)
                {
                    maxType = curType;
                    quality = numLevels;
                }
            }

            mParams.Multisample = maxType;
            mParams.MultisampleQuality = quality - 1;
        }

        /// <summary>
        /// Gets the native graphics device.
        /// </summary>
        public Device Device { get { return mDevice; } }

        /// <summary>
        /// Gets the currently active Camera
        /// </summary>
        public Camera Camera { get; private set; }

        /// <summary>
        /// Gets the currently active projection matrix.
        /// </summary>
        public Matrix ProjectionMatrix { get; private set; }

        /// <summary>
        /// Fired when a frame can be rendered.
        /// </summary>
        public event Action<Device> OnFrame;

        private PresentParameters mParams;
        private Device mDevice;
        private Direct3D mDriver;
    }
}
