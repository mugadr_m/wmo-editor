﻿using System;
using SlimDX;

namespace WMOEditor.Rendering
{
    public class Camera
    {
        /// <summary>
        /// Initializes the Camera to its default values (0/100/0) -> (0/99/0)
        /// </summary>
        public Camera()
        {
            mPosition = new Vector3(0.0f, 100.0f, 0.0f);
            mTarget = new Vector3(0.0f, 99.0f, 0.0f);
            mUp = new Vector3(0.0f, 0.0f, 1.0f);
            mRight = new Vector3(1.0f, 0.0f, 0.0f);

            mMatView = Matrix.LookAtLH(mPosition, mTarget, mUp);

            mLastUpdate = DateTime.Now;
            mLastCursorPos = System.Windows.Forms.Cursor.Position;
        }

        /// <summary>
        /// Updates the camera according to a set of keyboard and mouse inputs.
        /// </summary>
        public void Update()
        {
            var keyBuffer = new byte[256];
            NativeMethods.GetKeyboardState(keyBuffer);

            var diff = DateTime.Now - mLastUpdate;
            mLastUpdate = DateTime.Now;

            var changed = false;

            if ((keyBuffer['W'] & 0x80) != 0)
            {
                var dir = mTarget - mPosition;
                dir.Normalize();

                mTarget += (float)diff.TotalSeconds * 20.0f * dir;
                mPosition += (float)diff.TotalSeconds * 20.0f * dir;
                changed = true;
            }

            if ((keyBuffer['S'] & 0x80) != 0)
            {
                var dir = mTarget - mPosition;
                dir.Normalize();

                mTarget -= (float)diff.TotalSeconds * 20.0f * dir;
                mPosition -= (float)diff.TotalSeconds * 20.0f * dir;
                changed = true;
            }

            if ((keyBuffer['A'] & 0x80) != 0)
            {
                mTarget -= (float)diff.TotalSeconds * 20.0f * mRight;
                mPosition -= (float)diff.TotalSeconds * 20.0f * mRight;
                changed = true;
            }

            if ((keyBuffer['D'] & 0x80) != 0)
            {
                mTarget += (float)diff.TotalSeconds * 20.0f * mRight;
                mPosition += (float)diff.TotalSeconds * 20.0f * mRight;
                changed = true;
            }

            if ((keyBuffer['Q'] & 0x80) != 0)
            {
                mTarget += (float)diff.TotalSeconds * 20.0f * mUp;
                mPosition += (float)diff.TotalSeconds * 20.0f * mUp;
                changed = true;
            }

            if ((keyBuffer['E'] & 0x80) != 0)
            {
                mTarget -= (float)diff.TotalSeconds * 20.0f * mUp;
                mPosition -= (float)diff.TotalSeconds * 20.0f * mUp;
                changed = true;
            }

            var dx = System.Windows.Forms.Cursor.Position.X - mLastCursorPos.X;
            var dy = System.Windows.Forms.Cursor.Position.Y - mLastCursorPos.Y;

            mLastCursorPos = System.Windows.Forms.Cursor.Position;

            if (dx != 0 && (keyBuffer[2] & 0x80) != 0)
            {
                var matRot = Matrix.RotationAxis(Vector3.UnitZ, (dx * 0.00015f * 180.0f) / 3.141592f);
                var forward = mTarget - mPosition;
                forward.Normalize();

                forward = Vector3.TransformCoordinate(forward, matRot);
                forward.Normalize();

                mTarget = mPosition + forward;

                mUp = Vector3.TransformCoordinate(mUp, matRot);
                mRight = Vector3.TransformCoordinate(mRight, matRot);
                mUp.Normalize();
                mRight.Normalize();
                changed = true;
            }

            if (dy != 0 && (keyBuffer[2] & 0x80) != 0)
            {
                var matRot = Matrix.RotationAxis(mRight, (dy * 0.00015f * 180.0f) / 3.141592f);
                var forward = mTarget - mPosition;
                forward.Normalize();

                forward = Vector3.TransformCoordinate(forward, matRot);
                forward.Normalize();

                mTarget = mPosition + forward;

                mUp = Vector3.TransformCoordinate(mUp, matRot);
                mUp.Normalize();
                changed = true;
            }

            if (changed)
            {
                mMatView = Matrix.LookAtLH(mPosition, mTarget, mUp);
                mViewFrustum.BuildViewFrustum(mMatView, RenderLoop.Instance.Device.ProjectionMatrix);

                ViewChanged?.Invoke();
            }
        }

        /// <summary>
        /// Gets the view matrix associated to the camera.
        /// </summary>
        public Matrix ViewMatrix => mMatView;

        public Vector3 Position => mPosition;

        public event Action ViewChanged;

        internal Frustum ViewFrustum => mViewFrustum;

        private Matrix mMatView;
        private DateTime mLastUpdate;
        private System.Drawing.Point mLastCursorPos;

        private Vector3 mPosition, mTarget, mUp, mRight;
        private readonly Frustum mViewFrustum = new Frustum();
    }
}
