﻿using System;
using SlimDX.Direct3D9;

namespace WMOEditor.Rendering
{
    public class ShaderCode
    {
        /// <summary>
        /// Compiles a shader from a given code with an entry point to a selected profile
        /// </summary>
        /// <param name="code">The code of the shader</param>
        /// <param name="entryPoint">The entry point of the shader</param>
        /// <param name="profile">The destination profile</param>
        public ShaderCode(string code, string entryPoint, string profile)
        {
            try
            {
                string errors;
                mCode = ShaderBytecode.Compile(code, null, null, entryPoint, profile, ShaderFlags.SkipOptimization | ShaderFlags.Debug, out errors);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString());
            }
        }

        /// <summary>
        /// Creates a Direct3D9 vertex shader object from the independent shader code
        /// </summary>
        /// <returns>Direct3D9 vertex shader</returns>
        public VertexShader CreateVertexShader()
        {
            return new VertexShader(RenderLoop.Instance.Device.Device, mCode);
        }

        /// <summary>
        /// Creates a Direct3D9 pixel shader object from the independent shader code
        /// </summary>
        /// <returns>Direct3D9 pixel shader</returns>
        public PixelShader CreatePixelShader()
        {
            return new PixelShader(RenderLoop.Instance.Device.Device, mCode);
        }

        public ConstantTable ConstantTable => mCode.ConstantTable;

        private readonly ShaderBytecode mCode;
    }
}
