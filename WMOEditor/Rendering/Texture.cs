﻿using System;
using System.IO;
using SlimDX.Direct3D9;

namespace WMOEditor.Rendering
{
    public class Texture
    {
        /// <summary>
        /// Constructs a BLP texture from an MPQ file
        /// </summary>
        /// <param name="file">Handle to a MPQ file with the BLP texture</param>
        public Texture(Mpq.File file)
        {
            LoadBlpTexture(file);
        }

        /// <summary>
        /// Saves the texture to an image on the disk in JPG format.
        /// </summary>
        /// <param name="fileName">Destination file (should end with jpg)</param>
        public void SaveTo(string fileName)
        {
            BaseTexture.ToFile(mTexture, fileName, ImageFileFormat.Jpg);
        }

        /// <summary>
        /// Performs the load of a blp texture from a MPQ file and loads all the graphics data.
        /// </summary>
        /// <param name="fl">File containing the blp texture</param>
        private void LoadBlpTexture(Stream fl)
        {
            var reader = new BinaryReader(fl);
            var sig = reader.ReadUInt32();
            if (sig != 0x32504C42)
                throw new Exception("Invalid blp signature");

            reader.BaseStream.Position += 4;
            var compression = reader.ReadByte();
            reader.ReadByte();
            var alphaEncoding = reader.ReadByte();
            reader.ReadByte();
            var width = reader.ReadInt32();
            var height = reader.ReadInt32();
            var offsets = new int[16];
            var sizes = new int[16];
            var ofsTmp = reader.ReadBytes(16 * 4);
            var sizTmp = reader.ReadBytes(16 * 4);
            var levelCount = 0;
            var blockSize = 0;
            for (var i = 0; i < 16; ++i)
            {
                offsets[i] = BitConverter.ToInt32(ofsTmp, 4 * i);
                sizes[i] = BitConverter.ToInt32(sizTmp, 4 * i);
                if (offsets[i] != 0 && sizes[i] != 0)
                    ++levelCount;
            }

            var texFmt = Format.Unknown;
            if (compression == 2)
            {
                switch (alphaEncoding)
                {
                    case 0:
                        texFmt = Format.Dxt1;
                        blockSize = 2;
                        break;
                    case 1:
                        texFmt = Format.Dxt3;
                        blockSize = 4;
                        break;
                    case 7:
                        texFmt = Format.Dxt5;
                        blockSize = 4;
                        break;
                }
            }

            if (compression == 3)
            {
                texFmt = Format.A8R8G8B8;
                blockSize = 4;
            }

            if (texFmt == Format.Unknown)
                throw new FormatException("This format is not yet supported, sorry!");

            var texture = new SlimDX.Direct3D9.Texture(RenderLoop.Instance.Device.Device, width, height, levelCount,
                Usage.None, texFmt, Pool.Managed);
            var curLevel = 0;

            for (var i = 0; i < 16; ++i)
            {
                if (sizes[i] != 0 && offsets[i] != 0)
                {
                    reader.BaseStream.Position = offsets[i];
                    var layerData = reader.ReadBytes(sizes[i]);
                    var surf = texture.GetSurfaceLevel(curLevel);
                    var desc = texture.GetLevelDescription(curLevel);
                    var rec = System.Drawing.Rectangle.FromLTRB(0, 0, desc.Width, desc.Height);
                    Surface.FromMemory(surf, layerData, Filter.Triangle, 0, texFmt, blockSize * rec.Width, rec);
                    ++curLevel;
                }
            }

            mTexture = texture;
        }

        public SlimDX.Direct3D9.Texture Native { get { return mTexture; } }

        /// <summary>
        /// Native representation of the texture in Direct3D9
        /// </summary>
        private SlimDX.Direct3D9.Texture mTexture;
    }
}
