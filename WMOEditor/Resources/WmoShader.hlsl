
matrix matView;
matrix matProj;

float3 diffuseLight = float3(1, 1, 1);
float3 ambientLight = float3(0.1f, 0.1f, 0.1f);
float3 SunDirection = float3(1, 1, -1);

texture2D wmoTexture;

sampler wmoSampler = sampler_state
{
	Texture = <wmoTexture>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

struct VS_INPUT
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
};

struct PS_INPUT
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : TEXCOORD1;
	float4 Color : COLOR0;
};

float4 ApplySunLight(float4 base, float3 normal)
{
	float light = dot(normal, normalize(SunDirection));
	if(light < 0)
		light = 0;
	if(light > 0.5f)
		light = 0.5f + (light - 0.5f) * 0.65f;

	light += 0.3f;
	light = saturate(light);
	float3 diffuse = diffuseLight * light;
	diffuse += ambientLight;
	base.rgb *= light;

	return base;
}

PS_INPUT VertexMain(VS_INPUT vsInput)
{
	PS_INPUT ret = (PS_INPUT)0;

	float4 pos = mul(float4(vsInput.Position.xyz, 1.0f), matView);
	pos = mul(pos, matProj);

	ret.Position = pos;
	ret.TexCoord = vsInput.TexCoord;
	ret.Normal = vsInput.Normal;
	ret.Color = vsInput.Color;

	return ret;
}

float4 PixelMain(PS_INPUT psInput) : COLOR0
{
	float4 base = tex2D(wmoSampler, psInput.TexCoord);
	base *= psInput.Color;
	base = ApplySunLight(base, psInput.Normal);
	base = saturate(base);

	return base;
}