
matrix matView;
matrix matProj;

float3 diffuseLight = float3(1, 1, 1);
float3 ambientLight = float3(0.1f, 0.1f, 0.1f);
float3 SunDirection = float3(1, 1, -1);

texture2D modelTexture;

sampler modelSampler = sampler_state
{
	Texture = <modelTexture>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

struct VS_INPUT
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;

	float4 InstanceColor : COLOR1;
	float4 mat0 : TEXCOORD1;
	float4 mat1 : TEXCOORD2;
	float4 mat2 : TEXCOORD3;
	float4 mat3 : TEXCOORD4;
};

struct PS_INPUT
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : TEXCOORD1;
	float4 InstanceColor : COLOR0;
};

float4 ApplySunLight(float4 base, float3 normal)
{
	float light = dot(normal, normalize(SunDirection));
	if(light < 0)
		light = 0;
	if(light > 0.5f)
		light = 0.5f + (light - 0.5f) * 0.65f;

	light += 0.3f;
	light = saturate(light);
	float3 diffuse = diffuseLight * light;
	diffuse += ambientLight;
	base.rgb *= light;

	return base;
}

PS_INPUT VertexMain(VS_INPUT vsInput)
{
	PS_INPUT ret = (PS_INPUT)0;

	float4x4 modelMatrix = {
		vsInput.mat0,
		vsInput.mat1,
		vsInput.mat2,
		vsInput.mat3
	};

	float4 posTmp = float4(vsInput.Position.xyz, 1.0f);
	posTmp = mul(posTmp, modelMatrix);

	float3 instanceNorm = mul(float4(vsInput.Normal.xyz, 1.0f), modelMatrix);

	float4 pos = mul(posTmp, matView);
	pos = mul(pos, matProj);

	ret.Position = pos;
	ret.TexCoord = vsInput.TexCoord;
	ret.Normal = normalize(instanceNorm);
	ret.InstanceColor = vsInput.InstanceColor;

	return ret;
}

float4 PixelMain(PS_INPUT psInput) : COLOR0
{
	float4 base = tex2D(modelSampler, psInput.TexCoord);
	base = ApplySunLight(base, psInput.Normal);

	return base * psInput.InstanceColor;
}