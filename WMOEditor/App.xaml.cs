﻿using System;
using System.Windows;
using Microsoft.Win32;

namespace WMOEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App {
        /// <summary>
        /// Initializes the application, loading the MPQ files.
        /// </summary>
        /// <param name="e">unused</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            RegistryKey wowKey;
            if (IntPtr.Size == 8)
                wowKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WoW6432Node\Blizzard Entertainment\World of Warcraft");
            else
                wowKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Blizzard Entertainment\World of Warcraft");

            if (wowKey == null)
            {
                throw new Exception("Unable to find start path!");
            }

            var wowPath = wowKey.GetValue("InstallPath").ToString();

            //Mpq.FileManager.Instance.Init("D:\\Users\\Public\\Games\\World of Warcraft");
            //Mpq.FileManager.Instance.Init(@"D:\Program Files (x86)\World of Warcraft");
            Mpq.FileManager.Instance.Init(wowPath);
        }
    }
}
