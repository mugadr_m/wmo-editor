﻿using System;
using System.Runtime.InteropServices;

namespace WMOEditor
{
    public static class BinaryReaderExtension
    {
        /// <summary>
        /// Extension method: Reads a struct (StructLayout.Sequential) from a binary stream.
        /// </summary>
        /// <typeparam name="T">Type of structure to read</typeparam>
        /// <param name="reader">Instance of the binary reader</param>
        /// <returns>Instance of T where the members are filled from the binary stream</returns>
        public static unsafe T ReadStruct<T>(this System.IO.BinaryReader reader) where T : struct
        {
            var size = Marshal.SizeOf(typeof(T));

            var buffer = new byte[size];
            reader.Read(buffer, 0, size);

            fixed (byte* ptr = buffer)
                return (T)Marshal.PtrToStructure((IntPtr)ptr, typeof(T));
        }

        /// <summary>
        /// Extension method: Reads an array of struct values (StructLayout.Sequential) from a binary stream.
        /// </summary>
        /// <typeparam name="T">Type of structure to read</typeparam>
        /// <param name="reader">Instance of the binary reader</param>
        /// <param name="values">Destination array which will be filled</param>
        public static void ReadStructs<T>(this System.IO.BinaryReader reader, T[] values) where T : struct
        {
            if (values == null)
                return;

            var size = Marshal.SizeOf(typeof(T)) * values.Length;
            if (size == 0)
                return;

            var buffer = new byte[size];
            reader.Read(buffer, 0, size);

            var handle = GCHandle.Alloc(values, GCHandleType.Pinned);
            Marshal.Copy(buffer, 0, handle.AddrOfPinnedObject(), size);
            handle.Free();
        }
    }
}
