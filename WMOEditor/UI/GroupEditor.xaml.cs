﻿// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local

namespace WMOEditor.UI
{
    /// <summary>
    /// Interaction logic for GroupEditor.xaml
    /// </summary>
    public partial class GroupEditor {
        class GroupFlagEntry
        {
            public string FlagTitle { get; set; }
            public int Flag { get; set; }
            public Model.WmoGroup Group;

            public bool FlagSet { get { return (Group.header.flags & Flag) != 0; } set { if (value) Group.header.flags |= (uint)Flag; else Group.header.flags &= ~(uint)Flag; } }
        }

        public GroupEditor()
        {
            InitializeComponent();
        }

        public void LoadFromGroup(Model.WmoGroup group)
        {
            var entry = new[]
            {
                new GroupFlagEntry()
                {
                    Flag = 1,
                    FlagTitle = "Has MOBN and MOBR",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 4,
                    FlagTitle = "Has Vertex colors",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 8,
                    FlagTitle = "Is outdoors",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x200,
                    FlagTitle = "Has lights",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x400,
                    FlagTitle = "Has MPBV, MPBP, ... chunks",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x800,
                    FlagTitle = "Has doodads",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x1000,
                    FlagTitle = "Has water",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x2000,
                    FlagTitle = "Is indoors",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x20000,
                    FlagTitle = "Has MORI and MORB",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x40000,
                    FlagTitle = "Show skybox",
                    Group = group
                },
                new GroupFlagEntry()
                {
                    Flag = 0x80000,
                    FlagTitle = "Is not ocean",
                    Group = group
                },
            };

            foreach (var ge in entry)
                flagBox.Items.Add(ge);
        }
    }
}
