﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMOEditor.UI
{
    internal class DoodadSetEntry
    {
        public DoodadSetEntry(bool active, string setName, int numEntries, int set)
        {
            IsActive = active;
            Title = setName;
            Description = "Number of objects: " + numEntries;
            SetIndex = set;
        }

        public bool IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int SetIndex { get; set; }
    }
}
