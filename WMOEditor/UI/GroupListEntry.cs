﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WMOEditor.UI
{
    public partial class GroupListEntry
    {
        public GroupListEntry(Model.WmoGroup group)
        {
            DstGroup = group;
            BoundingBox = 
                "{ X: " + group.BoundingBox.Minimum.X.ToString("F2") + 
                " Y: " + group.BoundingBox.Minimum.Y.ToString("F2") +
                " Z: " + group.BoundingBox.Minimum.Z.ToString("F2") + " } - {" +
                " X: " + group.BoundingBox.Maximum.X.ToString("F2") + 
                " Y: " + group.BoundingBox.Maximum.Y.ToString("F2") +
                " Z: " + group.BoundingBox.Maximum.Z.ToString("F2") + " }";
        }

        public string Title { get; set; }
        public string BoundingBox { get; set; }

        public Model.WmoGroup DstGroup { get; set; }

        public void ItemCheckChanged(object sender, RoutedEventArgs args)
        {
            DstGroup.RenderEnabled = (sender as CheckBox).IsChecked.Value;
        }
    }
}
