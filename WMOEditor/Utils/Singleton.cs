﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMOEditor.Utils
{
    /// <summary>
    /// Default policy. Instance is created on type initialization.
    /// </summary>
    /// <typeparam name="T">Type of the singleton</typeparam>
    public class SingletonPolicy<T> where T : new()
    {
        /// <summary>
        /// Should be called on sigleton type initialization. Creates the instance.
        /// </summary>
        public virtual void InitInstance()
        {
            mInstance = new T();
        }

        /// <summary>
        /// Handles the request for an instance. The default policy just returns the instance.
        /// </summary>
        /// <returns>The singleton instance</returns>
        public virtual T GetInstance()
        {
            return mInstance;
        }

        protected T mInstance;
    }

    /// <summary>
    /// On demand policy. Creates the instance when its first accessed rather than on type initialization
    /// </summary>
    /// <typeparam name="T">Type of the singleton</typeparam>
    public class DemandPolicy<T> : SingletonPolicy<T> where T : new()
    {
        /// <summary>
        /// Empty method, no action is performed on type initialization.
        /// </summary>
        public override void InitInstance()
        {

        }

        /// <summary>
        /// In the first call the instance is created, subsequent calls return said instance.
        /// </summary>
        /// <returns>The singleton instance</returns>
        public override T GetInstance()
        {
            if (mInstance == null)
                mInstance = new T();

            return mInstance;
        }
    }

    public class Singleton<T, U> where T : new() where U : SingletonPolicy<T>, new()
    {
        /// <summary>
        /// Performs actions depending on the selected singleton policy.
        /// </summary>
        static Singleton()
        {
            mPolicy = new U();
            mPolicy.InitInstance();
        }

        private static U mPolicy;

        /// <summary>
        /// Gets the singleton instance of this type.
        /// </summary>
        public static T Instance { get { return mPolicy.GetInstance(); } }
    }
}
