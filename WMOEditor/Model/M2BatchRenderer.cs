﻿using System.Collections.Generic;
using System.Text;
using SlimDX.Direct3D9;
using System.Runtime.InteropServices;

namespace WMOEditor.Model
{
    public class M2BatchRenderer
    {
        public M2BatchRenderer(string fileName)
        {
            mFile = new M2File(fileName);
            mFile.Load();

            var dev = Rendering.RenderLoop.Instance.Device.Device;
            mInstanceStream = new VertexBuffer(dev, Marshal.SizeOf(typeof(ModelInstanceData)), Usage.None, VertexFormat.None, Pool.Managed);
            Rendering.RenderLoop.Instance.Device.Camera.ViewChanged += UpdateVisibility;
        }

        public void Unload()
        {
            mInstanceStream.Dispose();

            foreach (var pass in mFile.Passes)
            {
                pass.indexBuffer.Dispose();
            }
        }

        public void SetDirty()
        {
            mIsDirty = true;
        }

        public void RenderBatch()
        {
            if (mVisibleInstanceCount == 0  || mFile.vertexBuffer == null)
                return;

            if (mIsDirty)
            {
                UpdateVisibility();
                mIsDirty = false;
            }

            var dev = Rendering.RenderLoop.Instance.Device.Device;

            Program.Apply();

            dev.VertexDeclaration = VertexDeclaration;
            dev.SetStreamSource(0, mFile.vertexBuffer, 0, ModelVertex.Size);
            dev.SetStreamSourceFrequency(0, mVisibleInstanceCount, StreamSource.IndexedData);

            dev.SetStreamSource(1, mInstanceStream, 0, ModelInstanceData.Size);
            dev.SetStreamSourceFrequency(1, 1, StreamSource.InstanceData);

            foreach (var pass in mFile.Passes)
            {
                pass.SetRenderStates();
                dev.Indices = pass.indexBuffer;

                dev.SetTexture(0, pass.gxTexture.Native);

                dev.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, mFile.Vertices.Count, 0, pass.indices.Length / 3);
                pass.UnsetRenderStates();
                
            }

            dev.ResetStreamSourceFrequency(0);
            dev.ResetStreamSourceFrequency(1);
        }

        internal IEnumerable<M2BatchInstance> GetVisibleInstances()
        {
            return mVisibleInstances;
        }

        public void AddInstance(float x, float y, float z, float scale, SlimDX.Quaternion rot)
        {
            var rotation = SlimDX.Matrix.RotationQuaternion(rot);
            var scaling = SlimDX.Matrix.Scaling(scale, scale, scale);
            var translation = SlimDX.Matrix.Translation(x, y, z);
            var bi = new M2BatchInstance(mFile, translation, scaling, rotation);
            mBatchInstances.Add(bi);

            var instances = new List<ModelInstanceData>();

            mVisibleInstances.Clear();

            foreach (var instance in mBatchInstances)
            {
                if (instance.IsVisible)
                {
                    instances.Add(instance.InstanceData);
                    mVisibleInstances.Add(instance);
                }
            }

            mVisibleInstanceCount = instances.Count;
            if (mVisibleInstanceCount > 0)
            {
                mInstanceStream.Dispose();
                mInstanceStream = new VertexBuffer(Rendering.RenderLoop.Instance.Device.Device, mVisibleInstanceCount * ModelInstanceData.Size, Usage.None, VertexFormat.None, Pool.Managed);

                var strm = mInstanceStream.Lock(0, 0, LockFlags.None);
                strm.WriteRange(instances.ToArray());
                mInstanceStream.Unlock();
            }
        }

        private void UpdateVisibility()
        {
            var instances = new List<ModelInstanceData>();
            mVisibleInstances.Clear();
            foreach (var instance in mBatchInstances)
            {
                if (instance.IsVisible)
                {
                    instances.Add(instance.InstanceData);
                    mVisibleInstances.Add(instance);
                }
            }

            mVisibleInstanceCount = instances.Count;
            if (mVisibleInstanceCount > 0)
            {
                mInstanceStream.Dispose();
                mInstanceStream = new VertexBuffer(Rendering.RenderLoop.Instance.Device.Device, mVisibleInstanceCount * ModelInstanceData.Size, Usage.None, VertexFormat.None, Pool.Managed);

                var strm = mInstanceStream.Lock(0, 0, LockFlags.None);
                strm.WriteRange(instances.ToArray());
                mInstanceStream.Unlock();
            }
        }

        static M2BatchRenderer()
        {
            VertexElements = new[]
            {
                new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0),
                new VertexElement(0, 12, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.Color, 0),
                new VertexElement(0, 28, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
                new VertexElement(0, 36, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Normal, 0),

                new VertexElement(1, 0, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.Color, 1),
                new VertexElement(1, 16, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 1),
                new VertexElement(1, 32, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 2),
                new VertexElement(1, 48, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 3),
                new VertexElement(1, 64, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 4),

                VertexElement.VertexDeclarationEnd
            };

            VertexDeclaration = new VertexDeclaration(Rendering.RenderLoop.Instance.Device.Device, VertexElements);

            Program = new Rendering.ShaderProgram(
                new Rendering.ShaderCode(Encoding.ASCII.GetString(Shaders.M2Shader), "VertexMain", "vs_3_0"),
                new Rendering.ShaderCode(Encoding.ASCII.GetString(Shaders.M2Shader), "PixelMain", "ps_3_0"));
        }

        private bool mIsDirty;
        private readonly M2File mFile;
        private VertexBuffer mInstanceStream;
        private int mVisibleInstanceCount;
        private readonly List<M2BatchInstance> mBatchInstances = new List<M2BatchInstance>();
        private readonly List<M2BatchInstance> mVisibleInstances = new List<M2BatchInstance>();

        public static readonly VertexDeclaration VertexDeclaration;
        public static readonly VertexElement[] VertexElements;

        private static readonly Rendering.ShaderProgram Program;
    }
}
