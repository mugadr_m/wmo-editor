﻿using System.Text;
using System.Runtime.InteropServices;
// ReSharper disable InconsistentNaming

namespace WMOEditor.Model
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MOHD
    {
        public uint nMaterials, nGroups, nPortals, nLights, nModels, nDoodads, nSets;
        public uint unkColor;
        public uint wmoAreaId;
        public SlimDX.BoundingBox bbox;
        public uint unkLiquid;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOMT
    {
        public uint flags;
        public uint shader;
        public uint blendMode;
        public uint texture1;
        public uint color1;
        public uint flags1;
        public uint texture2;
        public uint color2;
        public uint flags2;
        public uint texture3;
        public uint color3;
        private uint unused1, unused2, unused3, unused4, unused5;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOGI
    {
        public uint flags;
        public SlimDX.BoundingBox bbox;
        public int nameOffset;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOPT
    {
        public short baseVertex;
        public short numVertices;
        public SlimDX.Vector3 unkVector;
        public float unk;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOPR
    {
        public ushort portalIndex;
        public ushort groupIndex;
        public short side;
        public ushort pad;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOLT
    {
        public byte LightType;
        public byte type;
        public bool useAttenuation;
        public byte pad;
        public uint color;
        public SlimDX.Vector3 position;
        public float intensity;
        public float attenuationStart;
        public float attenuationEnd;
        public float unk1, unk2, unk3, unk4;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MODS
    {
        public byte name1, name2, name3, name4, name5, name6;
        public byte name7, name8, name9, name10, name11, name12;
        public byte name13, name14, name15, name16, name17, name18;
        public byte name19, name20;
        public uint firstInstance;
        public uint numDoodads;
        public uint pad;

        public string GetSetName()
        {
            if (name1 == 0)
                return "";

            byte[] strBytes = { name1, name2, name3, name4, name5, name6, name7, name8, name9,
                                  name10, name11, name12, name13, name14, name15, name16, name17,
                                  name18, name19, name20 };

            var name = Encoding.ASCII.GetString(strBytes);
            var nullBytes = name.IndexOf('\0');
            name = name.Remove(nullBytes);
            return name;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MODD
    {
        public uint nameIndex;
        public SlimDX.Vector3 position;
        public SlimDX.Vector4 rotation;
        public float scale;
        public uint color;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MFOG
    {
        public uint flags;
        public SlimDX.Vector3 position, start, end;
        public float fog1, fog2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOGP
    {
        public uint nameStart, descStart, flags;
        public SlimDX.BoundingBox bbox;
        public ushort portalStart, portalCount;
        public short batchesA, batchesB, batchesC, batchesD;
        public byte fog1, fog2, fog3, fog4;
        public uint liquidType, groupAreaId, pad1, pad2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOPY
    {
        public byte flags;
        public byte material;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOBA
    {
        public short plane1, plane2, plane3, plane4, plane5, plane6;
        public uint startIndex;
        public ushort numIndices;
        public ushort startVertex, numVertices;
        public byte unk1;
        public byte material;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOBN
    {
        public short planeType;
        public short childLeft, childRight;
        public ushort numFaces;
        public uint firstFace;
        public float dist;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MLIQ
    {
        public uint xverts, yverts, xtiles, ytiles;
        public SlimDX.Vector3 startPos;
        public ushort materialId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct WmoVertex
    {
        public SlimDX.Vector3 Position;
        public SlimDX.Color4 Color;
        public SlimDX.Vector2 TexCoord;
        public SlimDX.Vector3 Normal;

        public static int Size = Marshal.SizeOf(typeof(WmoVertex));
    }
}
