﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WMOEditor.Model
{
    public class M2File
    {
        /// <summary>
        /// Opens a M2 model from a file in the MPQ system without loading it.
        /// </summary>
        /// <param name="fileName">MPQ path to the file</param>
        public M2File(string fileName)
        {
            FullName = fileName;
            Passes = new List<M2RenderPass>();
            FileDirectory = System.IO.Path.GetDirectoryName(fileName);
            mFile = new Mpq.File(fileName);
            mReader = new System.IO.BinaryReader(mFile);
        }

        /// <summary>
        /// Loads all the data for the model necessary to render the model.
        /// </summary>
        public void Load()
        {
            mHeader = mReader.ReadStruct<M2Header>();

            var tmp = mHeader.BoundingBox.Maximum;
            mHeader.BoundingBox.Maximum = mHeader.BoundingBox.Minimum;
            mHeader.BoundingBox.Minimum = tmp;

            tmp = mHeader.VertexBox.Maximum;
            mHeader.VertexBox.Maximum = mHeader.VertexBox.Minimum;
            mHeader.VertexBox.Minimum = tmp;

            mFile.Position = mHeader.ofsName;

            mModelName = ReadString(mHeader.lenName);

            LoadTextures();
            LoadVertices();
            LoadSkins();

            mReader.Dispose();
        }

        public bool IntersectRay(SlimDX.Ray ray, out float distance)
        {
            distance = 0;
            if (vertexBuffer == null)
                return false;

            var hasHit = false;

            var curNearDist = float.MaxValue;

            foreach (var pass in Passes)
            {
                float dist;
                var hit = pass.RayHitTest(ray, out dist);
                if (hit)
                {
                    if (dist < curNearDist)
                        curNearDist = dist;

                    hasHit = true;
                }
            }

            distance = curNearDist;
            return hasHit;
        }

        /// <summary>
        /// Reads a number of bytes from the current position of the file and interprets them as a 0-terminated
        /// UTF-8 string.
        /// </summary>
        /// <param name="maxLen">Maximum number of bytes to read (or till a 0 is reached)</param>
        /// <returns>0-terminated string at the current position</returns>
        private string ReadString(uint maxLen = 0)
        {
            var strBytes = new List<byte>();

            var b = mReader.ReadByte();
            while (b != 0 && (maxLen == 0 || strBytes.Count < maxLen))
            {
                strBytes.Add(b);
                b = mReader.ReadByte();
            }

            return Encoding.UTF8.GetString(strBytes.ToArray());
        }

        /// <summary>
        /// Loads all data related to textures.
        /// </summary>
        private void LoadTextures()
        {
            mFile.Position = mHeader.ofsTextures;

            var textures = new M2Texture[mHeader.nTextures];
            mReader.ReadStructs(textures);

            mTextureInfo = textures.ToList();

            foreach (var texInfo in mTextureInfo)
            {
                mFile.Position = texInfo.ofsFileName;

                var str = ReadString(texInfo.lenFileName);
                mTextureNames.Add(str);
            }
        }

        private void LoadVertices()
        {
            mFile.Position = mHeader.ofsVertices;

            var vertices = new M2Vertex[mHeader.nVertices];
            mReader.ReadStructs(vertices);

            foreach (var v in vertices)
            {
                var mv = new ModelVertex()
                {
                    Color = System.Drawing.Color.White,
                    Normal = v.Normal,
                    Position = v.Position,
                    TexCoord = v.TexCoord
                };

                mVertices.Add(mv);
            }

            if (mVertices.Count > 0)
            {
                vertexBuffer = new SlimDX.Direct3D9.VertexBuffer(
                    Rendering.RenderLoop.Instance.Device.Device,
                    mVertices.Count * ModelVertex.Size, SlimDX.Direct3D9.Usage.None, SlimDX.Direct3D9.VertexFormat.None,
                    SlimDX.Direct3D9.Pool.Managed);

                var strm = vertexBuffer.Lock(0, 0, SlimDX.Direct3D9.LockFlags.None);
                strm.WriteRange(mVertices.ToArray());
                vertexBuffer.Unlock();
            }
            else
                vertexBuffer = null;
        }

        private void LoadSkins()
        {
            var skinFile = FileDirectory + '\\' + ModelName + "00.skin";
            var skin = new Mpq.File(skinFile);

            var mView = skin.Read<SKINView>();
            var indexLookup = new ushort[mView.nIndices];
            skin.Position = mView.ofsIndices;
            skin.Read(indexLookup);
            var triangles = new ushort[mView.nTriangles];
            skin.Position = mView.ofsTriangles;
            skin.Read(triangles);

            var subMeshes = new SKINSubMesh[mView.nSubMeshes];
            skin.Position = mView.ofsSubMeshes;
            skin.Read(subMeshes);

            var texUnits = new SKINTexUnit[mView.nTexUnits];
            skin.Position = mView.ofsTexUnits;
            skin.Read(texUnits);

            var texLookUp = new ushort[mHeader.nTexLookup];
            mFile.Position = mHeader.ofsTexLookup;
            mFile.Read(texLookUp);

            var texUnitLookUp = new ushort[mHeader.nTexUnits];
            mFile.Position = mHeader.ofsTexUnits;
            mFile.Read(texUnitLookUp);

            var renderFlags = new M2RenderFlags[mHeader.nRenderFlags];
            mFile.Position = mHeader.ofsRenderFlags;
            mFile.Read(renderFlags);

            var indices = new ushort[mView.nTriangles];
            for (var i = 0; i < mView.nTriangles; ++i)
                indices[i] = indexLookup[triangles[i]];

            for (var i = 0; i < mView.nTexUnits; ++i)
            {
                var pass = new M2RenderPass();
                var mesh = subMeshes[texUnits[i].SubMesh1];
                pass.indices = new ushort[mesh.nTriangles];
                pass.texture = mTextureNames[texLookUp[texUnits[i].Texture]];
                pass.blendMode = renderFlags[texUnits[i].RenderFlags];
                pass.parentModel = this;

                for (ushort t = mesh.startTriangle, k = 0; k < mesh.nTriangles; ++t, ++k)
                {
                    var index = indices[t];
                    pass.indices[k] = index;
                }

                pass.LoadMesh();

                Passes.Add(pass);
            }
        }

        public string FullName { get; private set; }
        public string FileDirectory { get; }
        public string ModelName => mModelName;
        public List<M2RenderPass> Passes { get; }
        public List<ModelVertex> Vertices => mVertices;
        public SlimDX.BoundingBox BoundingBox => mHeader.VertexBox;

        public SlimDX.Direct3D9.VertexBuffer vertexBuffer;

        private M2Header mHeader;
        private readonly Mpq.File mFile;
        private readonly System.IO.BinaryReader mReader;
        private string mModelName;
        private List<M2Texture> mTextureInfo;
        private readonly List<ModelVertex> mVertices = new List<ModelVertex>();
        private readonly List<string> mTextureNames = new List<string>();
    }
}
