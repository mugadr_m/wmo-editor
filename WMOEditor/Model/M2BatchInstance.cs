﻿using SlimDX;

namespace WMOEditor.Model
{
    internal class M2BatchInstance
    {
        private ModelInstanceData mInstanceData;
        private M2File mModel;

        public M2BatchInstance(M2File file, Matrix trans, Matrix scale, Matrix rot)
        {
            mModel = file;
            Matrix.Invert(trans);
            mInstanceData = new ModelInstanceData() { ModelMatrix = rot * scale * trans, InstanceColor = System.Drawing.Color.White };
        }

        public void SetSelected(bool selected)
        {
            mInstanceData.InstanceColor = selected ? new Color4(1.0f, 1.0f, 0.5f, 0.5f) : System.Drawing.Color.White;
            M2Manager.GetRenderer(mModel.FullName).SetDirty();
        }

        public bool RayHitTest(System.Drawing.Point pt, out float distance, out Vector3 hitPos)
        {
            var rayTransformed = CalcRayForTransform(pt, Matrix.Invert(mInstanceData.ModelMatrix));

            var hit = mModel.IntersectRay(rayTransformed, out distance);

            hitPos = rayTransformed.Position + distance * rayTransformed.Direction;
            hitPos = Vector3.TransformCoordinate(hitPos, mInstanceData.ModelMatrix);

            distance = (hitPos - Rendering.RenderLoop.Instance.Device.Camera.Position).Length();

            return hit;
        }

        private Ray CalcRayForTransform(System.Drawing.Point pt, Matrix mat)
        {
            var matView = Rendering.RenderLoop.Instance.Device.Camera.ViewMatrix;
            var matProj = Rendering.RenderLoop.Instance.Device.ProjectionMatrix;
            var viewport = Rendering.RenderLoop.Instance.Device.Device.Viewport;

            var screenCoord = new Vector3();
            screenCoord.X = (((2.0f * pt.X) / viewport.Width) - 1);
            screenCoord.Y = -(((2.0f * pt.Y) / viewport.Height) - 1);

            var invProj = Matrix.Invert(matProj);
            var invView = Matrix.Invert(matView);

            var nearPos = new Vector3(screenCoord.X, screenCoord.Y, 0);
            var farPos = new Vector3(screenCoord.X, screenCoord.Y, 1);

            nearPos = Vector3.TransformCoordinate(nearPos, invProj * invView * mat);
            farPos = Vector3.TransformCoordinate(farPos, invProj * invView * mat);

            return new Ray(nearPos, Vector3.Normalize((farPos - nearPos)));
        }

        public bool IsVisible => Rendering.RenderLoop.Instance.Device.Camera.ViewFrustum.Contains(mModel.BoundingBox, mInstanceData.ModelMatrix) != ContainmentType.Disjoint;

        public ModelInstanceData InstanceData => mInstanceData;
    }
}
