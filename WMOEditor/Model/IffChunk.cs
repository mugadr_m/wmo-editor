﻿using System.IO;

namespace WMOEditor.Model
{
    public class IffChunk
    {
        /// <summary>
        /// Gets the 4-byte identifier of the chunk
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Gets the number of bytes of sub data of the chunk
        /// </summary>
        public uint Size { get; set; }
        /// <summary>
        /// Gets the stream of the sub data of the chunk
        /// </summary>
        public MemoryStream Data { get; set; }
        /// <summary>
        /// Gets a binary reader to read the sub data of the chunk
        /// </summary>
        public BinaryReader DataReader { get; set; }

        /// <summary>
        /// Interprets the content of the chunk as a certain type.
        /// </summary>
        /// <typeparam name="T">Type of the data</typeparam>
        /// <returns>An instance of T filled with the sub data of the chunk</returns>
        public T As<T>() where T : struct
        {
            DataReader.BaseStream.Position = 0;
            return DataReader.ReadStruct<T>();
        }

        /// <summary>
        /// Reads an IffChunk from a binary stream
        /// </summary>
        /// <param name="reader">The binary input stream</param>
        /// <returns>An IffChunk with its sub data and information</returns>
        public static IffChunk Read(BinaryReader reader)
        {
            var ret = new IffChunk();
            ret.Id = reader.ReadUInt32();
            ret.Size = reader.ReadUInt32();

            var data = new byte[ret.Size];
            reader.Read(data, 0, data.Length);

            ret.Data = new MemoryStream(data, false);
            ret.Data.Position = 0;

            ret.DataReader = new BinaryReader(ret.Data);
            return ret;
        }
    }
}
