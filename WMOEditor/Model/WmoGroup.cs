﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.Direct3D9;

namespace WMOEditor.Model
{
    public class WmoGroup
    {
        /// <summary>
        /// Constructs a WMO group from a file and assigns an optional groupName to the group.
        /// </summary>
        /// <param name="parent">The parent root of this group</param>
        /// <param name="fileName">The file name of the group file</param>
        /// <param name="groupName">Optional name for the group</param>
        public WmoGroup(WmoRoot parent, string fileName, string groupName)
        {
            mFile = new Mpq.File(fileName);
            mParent = parent;
            mGroupName = groupName;
            RenderEnabled = true;
        }

        /// <summary>
        /// Renders the WMO group according to the current properties
        /// </summary>
        public void Render()
        {
            if (RenderEnabled == false)
                return;

            if (Rendering.RenderLoop.Instance.Device.Camera.ViewFrustum.Contains(header.bbox, SlimDX.Matrix.Identity)
                == SlimDX.ContainmentType.Disjoint)
                return;

            mRenderer.Render();
        }

        /// <summary>
        /// Converts this instance into a readable representation.
        /// </summary>
        /// <returns>A string for this instance</returns>
        public override string ToString()
        {
            return mGroupName;
        }

        /// <summary>
        /// Releases all unmanaged resources from this WMO group.
        /// </summary>
        public void Unload()
        {
            mRenderer.Unload();
        }

        /// <summary>
        /// Loads all data associated with the WMO group
        /// </summary>
        public void Load()
        {
            var reader = new System.IO.BinaryReader(mFile);

            try
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    var ic = IffChunk.Read(reader);
                    mIffChunks.Add(ic.Id, ic);
                }
            }
            catch (Exception) {
                // ignored
            }

            if (!HasIffChunk("MOGP"))
                throw new Exception("Missing MOGP chunk in group file");

            var cnk = GetIffChunk("MOGP");
            header = cnk.As<MOGP>();
            var tmp = header.bbox.Minimum;
            header.bbox.Minimum = header.bbox.Maximum;
            header.bbox.Maximum = tmp;

            try
            {
                while (cnk.Data.Position < cnk.Data.Length)
                {
                    var ic = IffChunk.Read(cnk.DataReader);
                    mIffChunks.Add(ic.Id, ic);
                }
            }
            catch (Exception) {
                // ignored
            }

            if (HasIffChunk("MODR"))
            {
                cnk = GetIffChunk("MODR");
                var indices = new ushort[cnk.Size / 2];
                cnk.DataReader.ReadStructs(indices);
                foreach (var index in indices)
                    mParent.LoadModelInstance(index);
            }

            LoadVertices();
            LoadBatches();

            mRenderer = new WmoGroupRender(this);
            mRenderer.Load();
        }

        public void OnVisibleSetChanged()
        {
            if (HasIffChunk("MODR"))
            {
                var cnk = GetIffChunk("MODR");
                var indices = new ushort[cnk.Size / 2];
                cnk.DataReader.BaseStream.Position = 0;
                cnk.DataReader.ReadStructs(indices);
                foreach (var index in indices)
                    mParent.LoadModelInstance(index);
            }
        }

        /// <summary>
        /// Applies all the information of a material to the stage.
        /// </summary>
        /// <param name="material">Material index</param>
        public void ApplyMaterial(byte material)
        {
            var tex = mParent.GetTexture(material);

            Rendering.RenderLoop.Instance.Device.Device.SetTexture(0, tex.Native);
            
            var dev = Rendering.RenderLoop.Instance.Device.Device;

            var mat = mParent.Materials[material];

            if (mat.blendMode != 0)
            {
                dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                dev.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
                dev.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
                dev.SetRenderState(RenderState.AlphaTestEnable, true);
                dev.SetRenderState(RenderState.AlphaRef, 1.0f / 255.0f);
                dev.SetRenderState(RenderState.AlphaFunc, Compare.GreaterEqual);
            }
            else
            {
                dev.SetRenderState(RenderState.AlphaBlendEnable, false);
                dev.SetRenderState(RenderState.AlphaTestEnable, false);
            }
        }

        /// <summary>
        /// Loads all the data related to vertices.
        /// </summary>
        private void LoadVertices()
        {
            if (!HasIffChunk("MOVT"))
                throw new Exception("Missing MOVT chunk!");

            var vtCnk = GetIffChunk("MOVT");
            var texCnk = GetIffChunk("MOTV");
            var norCnk = GetIffChunk("MONR");

            var numVertices = (int)vtCnk.Size / 12;
            var vertices = new SlimDX.Vector3[numVertices];
            var texCoords = new SlimDX.Vector2[numVertices];
            var normals = new SlimDX.Vector3[numVertices];
            var colorData = new byte[numVertices * 4];

            var hasClrCnk = HasIffChunk("MOCV") && ((header.flags & 0x2000) != 0);
            if (hasClrCnk)
            {
                var clrCnk = GetIffChunk("MOCV");
                clrCnk.DataReader.ReadStructs(colorData);
            }

            vtCnk.DataReader.ReadStructs(vertices);
            texCnk.DataReader.ReadStructs(texCoords);
            norCnk.DataReader.ReadStructs(normals);

            var index = 0;

            foreach(var vert in vertices)
            {
                SlimDX.Color4 color;
                if (!hasClrCnk)
                    color = System.Drawing.Color.White;
                else
                    color = new SlimDX.Color4(colorData[index * 4 + 3] / 255.0f, colorData[index * 4 + 2] / 255.0f, colorData[index * 4 + 2] / 255.0f, colorData[index * 4] / 255.0f);   
                
                var wmov = new WmoVertex()
                {
                    Position = vert,
                    Color = color,
                    TexCoord = texCoords[index],
                    Normal = normals[index],
                };

                mVertices.Add(wmov);
                ++index;
            }

            if (!HasIffChunk("MOVI"))
                throw new Exception("Missing MOVI chunk!");

            var viCnk = GetIffChunk("MOVI");

            var numIndices = (int)viCnk.Size / 2;

            var indices = new ushort[numIndices];
            viCnk.DataReader.ReadStructs(indices);

            mIndices = indices.ToList();
        }

        /// <summary>
        /// Loads all information related to the batches
        /// </summary>
        private void LoadBatches()
        {
            if (!HasIffChunk("MOBA"))
                throw new Exception("Missing MOBA chunk!");

            var baCnk = GetIffChunk("MOBA");

            var numBatches = (int)baCnk.Size / 24;

            var batches = new MOBA[numBatches];
            baCnk.DataReader.ReadStructs(batches);

            var textures = new List<string>();
            var materials = new List<MOMT>();
            foreach (var batch in batches)
            {
                textures.Add(mParent.GetTextureName(batch.material));
                materials.Add(mParent.GetMaterial(batch.material));
            }

            mBatches = batches.ToList();
        }

        /// <summary>
        /// Checks if a certain IFF chunk is present in the WMO
        /// </summary>
        /// <param name="id">4-byte id of the chunk</param>
        /// <returns>true if the chunks is contained, false otherwise</returns>
        private bool HasIffChunk(string id)
        {
            if (id.Length != 4)
                return false;

            var bytes = Encoding.ASCII.GetBytes(id);
            var sig = (uint)((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3]);
            return mIffChunks.ContainsKey(sig);
        }

        /// <summary>
        /// Retreives an IFF chunk by its 4-byte identifier
        /// </summary>
        /// <param name="id">4 byte identifier of the chunk</param>
        /// <returns>The chunk with the selected id</returns>
        private IffChunk GetIffChunk(string id)
        {
            if (id.Length != 4)
                throw new InvalidOperationException("Invalid signature for chunk");

            var bytes = Encoding.ASCII.GetBytes(id);
            var sig = (uint)((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3]);

            return mIffChunks[sig];
        }

        public List<WmoVertex> Vertices => mVertices;
        public List<ushort> Indices => mIndices;
        public List<MOBA> Batches => mBatches;
        public SlimDX.BoundingBox BoundingBox => header.bbox;

        public bool RenderEnabled { private get; set; }
        public MOGP header;

        private readonly WmoRoot mParent;
        private readonly Mpq.File mFile;
        private readonly string mGroupName;
        private WmoGroupRender mRenderer;

        private readonly List<WmoVertex> mVertices = new List<WmoVertex>();
        private List<ushort> mIndices = new List<ushort>();
        private List<MOBA> mBatches = new List<MOBA>();

        private readonly Dictionary<uint, IffChunk> mIffChunks = new Dictionary<uint, IffChunk>();
    }
}
