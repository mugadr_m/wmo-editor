﻿using System.Collections.Generic;
using SlimDX;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace WMOEditor.Model
{
    internal class ModelPickParameters
    {
        public float Distance { get; set; }
        public Vector3 HitPoint { get; set; }
        public bool HasHit { get; set; }
        public M2BatchInstance InstanceHit { get; set; }

        public System.Drawing.Point PickPosition { get; set; }
    }

    internal class ModelPicker
    {
        public static void PickModel(ModelPickParameters parameters)
        {
            var dist = float.MaxValue;
            var hasHit = false;
            var tmpInstances = new List<M2BatchInstance>();

            foreach (var batch in M2Manager.GetAllRenderer())
            {
                foreach (var instance in batch.GetVisibleInstances())
                {
                    tmpInstances.Add(instance);

                    Vector3 hitPoint;
                    float curDist;
                    var hit = instance.RayHitTest(parameters.PickPosition, out curDist, out hitPoint);
                    if (hit)
                    {
                        hasHit = true;
                        if (curDist < dist)
                        {
                            dist = curDist;
                            parameters.HitPoint = hitPoint;
                            parameters.InstanceHit = instance;
                        }
                    }
                }
            }

            foreach (var instance in tmpInstances)
            {
                instance.SetSelected(instance == parameters.InstanceHit);
            }

            parameters.Distance = dist;
            parameters.HasHit = hasHit;
        }
    }
}
