﻿using System.Linq;
using SlimDX;
using SlimDX.Direct3D9;
using System.Runtime.InteropServices;

namespace WMOEditor.Model
{
    /// <summary>
    /// Contains all neccessary information for the M2Render-class in the Video-namespace to render a part of the M2
    /// </summary>
    public class M2RenderPass
    {
        public void LoadMesh()
        {
            indexBuffer = new IndexBuffer(Rendering.RenderLoop.Instance.Device.Device,
                indices.Length * 2, Usage.None, Pool.Managed, true);

            var strm = indexBuffer.Lock(0, 0, LockFlags.None);
            strm.WriteRange(indices);
            indexBuffer.Unlock();

            gxTexture = Rendering.TextureManager.Instance.GetTexture(texture);

            mesh = new Mesh(Rendering.RenderLoop.Instance.Device.Device,
                indices.Length / 3, parentModel.Vertices.Count, MeshFlags.VertexBufferManaged | MeshFlags.IndexBufferManaged,
                M2BatchRenderer.VertexElements.Where(e => e.Stream == 0).ToList().Concat(new[] { VertexElement.VertexDeclarationEnd }).ToArray());

            strm = mesh.LockVertexBuffer(LockFlags.None);
            strm.WriteRange(parentModel.Vertices.ToArray());
            mesh.UnlockVertexBuffer();

            strm = mesh.LockIndexBuffer(LockFlags.None);
            strm.WriteRange(indices);
            mesh.UnlockIndexBuffer();
        }

        public bool RayHitTest(Ray ray, out float distance)
        {
            return mesh.Intersects(ray, out distance);
        }

        public void SetRenderStates()
        {
            var dev = Rendering.RenderLoop.Instance.Device.Device;
            switch (blendMode.blend)
            {
                case 0:
                    {
                        dev.SetRenderState(RenderState.AlphaTestEnable, true);
                        dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                        dev.SetRenderState(RenderState.AlphaFunc, Compare.Greater);
                        dev.SetRenderState(RenderState.AlphaRef, 0.01f);
                        break;
                    }
                case 1:
                    {
                        dev.SetRenderState(RenderState.AlphaTestEnable, true);
                        dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                        break;
                    }
                case 2:
                    {
                        dev.SetRenderState(RenderState.AlphaTestEnable, true);
                        dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                        dev.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
                        dev.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
                        break;
                    }
                case 3:
                    {
                        dev.SetRenderState(RenderState.AlphaTestEnable, true);
                        dev.SetRenderState(RenderState.DestinationBlend, Blend.One);
                        dev.SetRenderState(RenderState.SourceBlend, Blend.SourceColor);
                        break;
                    }
                case 4:
                    {
                        dev.SetRenderState(RenderState.AlphaTestEnable, true);
                        dev.SetRenderState(RenderState.DestinationBlend, Blend.One);
                        dev.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
                        break;
                    }

                case 6:
                case 5:
                    {
                        dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                        dev.SetRenderState(RenderState.SourceBlend, Blend.DestinationColor);
                        dev.SetRenderState(RenderState.DestinationBlend, Blend.SourceColor);
                    }
                    break;

                default:
                    {
                        dev.SetRenderState(RenderState.AlphaBlendEnable, true);
                        dev.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
                        dev.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
                    }
                    break;
            }
        }

        public void UnsetRenderStates()
        {
            var dev = Rendering.RenderLoop.Instance.Device.Device;
            dev.SetRenderState(RenderState.AlphaTestEnable, false);
            dev.SetRenderState(RenderState.AlphaBlendEnable, true);
            dev.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            dev.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
        }

        public IndexBuffer indexBuffer;
        public Mesh mesh;

        public ushort[] indices;
        /// <summary>
        /// The texture used in this pass
        /// </summary>
        public string texture;

        public Rendering.Texture gxTexture;

        public M2RenderFlags blendMode;

        public M2File parentModel;

        [StructLayout(LayoutKind.Sequential)]
        private struct IntersectStruct
        {
            public Vector3 v0, v1, v2;
            public Vector3 rayPos, rayDir;
            public float u, v, dist;
        }

        private static IntersectStruct mIntersectStruct = new IntersectStruct();


        static M2RenderPass() {
            var mIntersectGcHandle = GCHandle.Alloc(mIntersectStruct, GCHandleType.Pinned);
            mIntersectGcHandle.AddrOfPinnedObject();
        }
    }
}
