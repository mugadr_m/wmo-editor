﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using SlimDX;

namespace WMOEditor.Model
{
    [StructLayout(LayoutKind.Sequential)]
    public struct M2Header
    {
        public uint Magic, version, lenName, ofsName, globalFlags;
        public uint nGlobalSeq, ofsGlobalSeq, nAnimations, ofsAnimations;
        public uint nAnimLookup, ofsAnimLookup, nBones, ofsBones;
        public uint nKeyBoneLookup, ofsKeyBoneLookup, nVertices, ofsVertices;
        public uint nViews, nColors, ofsColors, nTextures, ofsTextures;
        public uint nTransparencies, ofsTransparencies, nUVAnims, ofsUVAnims;
        public uint nTexReplace, ofsTexReplace, nRenderFlags, ofsRenderFlags;
        public uint nBoneLookup, ofsBoneLookup, nTexLookup, ofsTexLookup;
        public uint nTexUnits, ofsTexUnits, nTransLookup, ofsTransLookup;
        public uint nUVAnimLookup, ofsUVAnimLookup;
        public BoundingBox VertexBox;
        public float VertexRadius;
        public BoundingBox BoundingBox;
        public float boundingRadius;
        public uint nBoundingTris, ofsBoundingTris, nBoundingVerts, ofsBoundingVerts;
        public uint nBoundingNorms, ofsBoundingNorms, nAttachments, ofsAttachments;
        public uint nAttachLookup, ofsAttachLookup, nEvents, ofsEvents, nLights;
        public uint ofsLights, nCameras, ofsCameras, nCameraLookup, ofsCameraLookup;
        public uint nRibbons, ofsRibbons, nParticles, ofsParticles;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct M2Vertex
    {
        public Vector3 Position;
        public byte boneWeight1, boneWeight2, boneWeight3, boneWeight4;
        public byte boneIndex1, boneIndex2, boneIndex3, boneIndex4;
        public Vector3 Normal;
        public Vector2 TexCoord;
        public float pad1, pad2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct M2RenderFlags
    {
        public ushort flags, blend;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct M2Texture
    {
        public uint type, flags, lenFileName, ofsFileName;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ModelVertex
    {
        public SlimDX.Vector3 Position;
        public SlimDX.Color4 Color;
        public SlimDX.Vector2 TexCoord;
        public SlimDX.Vector3 Normal;

        public static int Size = Marshal.SizeOf(typeof(ModelVertex));
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SKINView
    {
        internal uint ID, nIndices, ofsIndices, nTriangles, ofsTriangles;
        internal uint nProperties, ofsProperties, nSubMeshes, ofsSubMeshes;
        internal uint nTexUnits, ofsTexUnits, nBones;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SKINSubMesh
    {
        internal uint ID;
        internal ushort startVertx, nVertices, startTriangle, nTriangles;
        internal ushort nBones, startBone, unk1, unk2;
        internal float MinX, MinY, MinZ;
        internal float MaxX, MaxY, MaxZ;
        internal float Radius;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SKINTexUnit
    {
        internal ushort flags, shading, SubMesh1, SubMesh2, ColorIndex;
        internal ushort RenderFlags, TexUnitNumber, Mode, Texture, TexUnit2;
        internal ushort Transparency, TextureAnim;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ModelInstanceData
    {
        internal SlimDX.Color4 InstanceColor;
        internal SlimDX.Matrix ModelMatrix;

        public static int Size = Marshal.SizeOf(typeof(ModelInstanceData));
    }
}
