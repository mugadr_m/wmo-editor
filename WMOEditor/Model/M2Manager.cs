﻿using System.Collections.Generic;

namespace WMOEditor.Model
{
    public static class M2Manager
    {
        public static M2BatchRenderer GetRenderer(string modelName)
        {
            var hash = modelName.ToLower().GetHashCode();
            if (mRenderer.ContainsKey(hash))
                return mRenderer[hash];

            var renderer = new M2BatchRenderer(modelName);
            mRenderer.Add(hash, renderer);
            return renderer;
        }

        public static void Clear()
        {
            foreach (var render in mRenderer.Values)
                render.Unload();

            mRenderer.Clear();
        }

        public static void Render()
        {
            foreach (var render in mRenderer.Values)
                render.RenderBatch();
        }

        public static Dictionary<int, M2BatchRenderer>.ValueCollection GetAllRenderer() { return mRenderer.Values; }

        private static Dictionary<int, M2BatchRenderer> mRenderer = new Dictionary<int, M2BatchRenderer>();
    }
}
