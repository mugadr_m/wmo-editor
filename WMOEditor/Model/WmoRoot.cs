﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WMOEditor.Model
{
    public class WmoRoot
    {
        /// <summary>
        /// Constructs the wmo model from a MPQ file. Does not perform any loading yet.
        /// </summary>
        /// <param name="file">File containing the wmo file</param>
        /// <param name="fileName">Name of the file of the WMO</param>
        public WmoRoot(string fileName, Mpq.File file)
        {
            M2Manager.Clear();

            mReader = new System.IO.BinaryReader(file);
            mRootPath = System.IO.Path.GetDirectoryName(fileName) + "\\";
            mModelName = System.IO.Path.GetFileNameWithoutExtension(fileName);
        }

        /// <summary>
        /// Renders the WMO and its groups according to the current rendering properties
        /// </summary>
        public void Render()
        {
            foreach (var group in mGroups)
                group.Render();

            M2Manager.Render();
        }

        /// <summary>
        /// Loads the WMO and all its group files.
        /// </summary>
        public void Load()
        {
            try
            {
                mReader.BaseStream.Position = 0;

                while (mReader.BaseStream.Position < mReader.BaseStream.Length)
                {
                    var ic = IffChunk.Read(mReader);
                    mIffChunks.Add(ic.Id, ic);
                }
            }
            catch (Exception) {
                // ignored
            }

            ValidateChunks();

            mHeader = GetIffChunk("MOHD").As<MOHD>();

            LoadDoodads();
            LoadMaterials();
            LoadGroups();
        }

        /// <summary>
        /// Translates an offset into the MOTX chunk to a texture string
        /// </summary>
        /// <param name="offset">Offset in the MOTX chunk</param>
        /// <returns>A string of the texture at the specified offset</returns>
        public string TranslateTextureOffset(uint offset)
        {
            var txChunk = GetIffChunk("MOTX");

            if (offset > txChunk.Size)
                throw new IndexOutOfRangeException("Offset not in MOTX chunk.");

            txChunk.Data.Position = offset;

            var strData = new List<byte>();
            while (txChunk.Data.Position < txChunk.Data.Length)
            {
                var b = txChunk.DataReader.ReadByte();
                if (b == 0)
                    break;

                strData.Add(b);
            }

            return Encoding.UTF8.GetString(strData.ToArray());
        }

        /// <summary>
        /// Translates an offset into the MOGN chunk to a group name
        /// </summary>
        /// <param name="offset">Offset in the MOGN chunk</param>
        /// <returns>A strig of the group name at the specified offset</returns>
        public string TranslateGroupName(uint offset)
        {
            var gnChunk = GetIffChunk("MOGN");

            if (offset > gnChunk.Size)
                throw new IndexOutOfRangeException("Offset not in MOGN chunk.");

            gnChunk.Data.Position = offset;

            var strData = new List<byte>();
            while (gnChunk.Data.Position < gnChunk.Data.Length)
            {
                var b = gnChunk.DataReader.ReadByte();
                if (b == 0)
                    break;

                strData.Add(b);
            }

            return Encoding.UTF8.GetString(strData.ToArray());
        }

        public void LoadModelInstance(uint id)
        {
            if (id >= mSelectedSet.firstInstance && id < mSelectedSet.firstInstance + mSelectedSet.numDoodads)
            {
                var instance = mDoodadInstances.ElementAt((int)id);
                var fileName = GetDoodadName(instance.nameIndex).ToUpper();
                fileName = fileName.Replace(".MDX", ".M2");
                fileName = fileName.Replace(".MDL", ".M2");
                var renderer = M2Manager.GetRenderer(fileName);
                renderer.AddInstance(instance.position.X, instance.position.Y, instance.position.Z, instance.scale,
                    new SlimDX.Quaternion(instance.rotation.X, instance.rotation.Y, instance.rotation.Z, instance.rotation.W));
            }
        }

        /// <summary>
        /// Gets the Direct3D texture associated with a material.
        /// </summary>
        /// <param name="material">Index into the MOMT list</param>
        /// <returns>The direct3d texture</returns>
        public Rendering.Texture GetTexture(byte material)
        {
            return mTextures[material];
        }

        /// <summary>
        /// Releases all unmanaged resources from this WMO
        /// </summary>
        public void Unload()
        {
            foreach (var group in mGroups)
                group.Unload();
        }

        /// <summary>
        /// Gets a doodad name from the MODN chunk by an index into the chunk.
        /// </summary>
        /// <param name="index">Index into the MODN chunk</param>
        /// <returns>Name at the index</returns>
        public string GetDoodadName(uint index)
        {
            var dnChunk = GetIffChunk("MODN");

            index = index & 0xFFFFFF;

            if (index > dnChunk.Size)
                throw new IndexOutOfRangeException("Offset not in MODN chunk.");

            dnChunk.Data.Position = index;

            var strData = new List<byte>();
            while (dnChunk.Data.Position < dnChunk.Data.Length)
            {
                var b = dnChunk.DataReader.ReadByte();
                if (b == 0)
                    break;

                strData.Add(b);
            }

            return Encoding.UTF8.GetString(strData.ToArray());
        }

        /// <summary>
        /// Loads a list with the names of all doodads in the order they appear in the MODD chunk.
        /// </summary>
        /// <returns>A list of doodad names</returns>
        public List<string> GetDoodadNames()
        {
            var ret = new List<string>();

            foreach (var doodad in mDoodadInstances)
            {
                ret.Add(GetDoodadName(doodad.nameIndex));
            }

            return ret;
        }

        public void ChangeSetIndex(int index)
        {
            if (index >= mDoodadSets.Count)
                return;

            mSelectedSet = mDoodadSets[index];
            M2Manager.Clear();

            foreach (var group in mGroups)
                group.OnVisibleSetChanged();
        }

        public string GetTextureName(byte material)
        {
            return mMaterialTextures[material];
        }

        public MOMT GetMaterial(byte material)
        {
            return mMaterials[material];
        }

        /// <summary>
        /// Loads all the material related information of the WMO.
        /// </summary>
        private void LoadMaterials()
        {
            if (mHeader.nMaterials == 0)
                return;

            var materials = new MOMT[mHeader.nMaterials];
            GetIffChunk("MOMT").DataReader.ReadStructs(materials);

            mMaterials = materials.ToList();

            foreach (var mat in materials)
            {
                var texName = TranslateTextureOffset(mat.texture1);
                mMaterialTextures.Add(texName);
                try
                {
                    mTextures.Add(Rendering.TextureManager.Instance.GetTexture(texName));
                }
                catch (Exception)
                {
                    mTextures.Add(Rendering.TextureManager.DefaultTexture);
                }
            }
        }

        /// <summary>
        /// Loads the group information and the group files.
        /// </summary>
        private void LoadGroups()
        {
            if (mHeader.nGroups == 0)
                return;

            var groupInfos = new MOGI[mHeader.nGroups];
            GetIffChunk("MOGI").DataReader.ReadStructs(groupInfos);

            var curIndex = 0;

            foreach (var groupInfo in groupInfos)
            {
                var groupName = "wmo_group_" + curIndex;

                if (groupInfo.nameOffset >= 0)
                {
                    groupName = TranslateGroupName((uint)groupInfo.nameOffset);
                }

                var groupFile = mRootPath + mModelName + "_" + curIndex.ToString("D3") + ".wmo";

                var wmoGroup = new WmoGroup(this, groupFile, groupName);
                wmoGroup.Load();

                mGroups.Add(wmoGroup);

                ++curIndex;
            }
        }

        /// <summary>
        /// Loads all the doodad related data
        /// </summary>
        private void LoadDoodads()
        {
            var ddCnk = GetIffChunk("MODD");

            var doodads = new MODD[mHeader.nDoodads];
            ddCnk.DataReader.ReadStructs(doodads);

            mDoodadInstances = doodads.ToList();

            var dnCnk = GetIffChunk("MODN");
            dnCnk.Data.Position = 0;

            var curOffset = 0;
            while (curOffset < dnCnk.Size)
            {
                var s = ReadString(dnCnk.DataReader);
                curOffset += s.Length + 1;

                if (s.Length > 1)
                {
                    s = s.ToUpper();
                    if (s.EndsWith(".M2") == false)
                    {
                        s = s.Replace(".MDX", ".M2");
                        s = s.Replace(".MDL", ".M2");
                    }

                    mModelNames.Add(s);
                }
            }

            var dsCnk = GetIffChunk("MODS");
            var sets = new MODS[mHeader.nSets];
            dsCnk.DataReader.ReadStructs(sets);

            mSelectedSet = sets[0];
            mDoodadSets = sets.ToList();
        }

        private string ReadString(System.IO.BinaryReader reader, uint maxLen = 0)
        {
            var strBytes = new List<byte>();

            var b = reader.ReadByte();
            while (b != 0 && (maxLen == 0 || strBytes.Count < maxLen))
            {
                strBytes.Add(b);

                b = reader.ReadByte();
            }

            return Encoding.UTF8.GetString(strBytes.ToArray());
        }

        /// <summary>
        /// Checks if all the chunks needed to load the WMO are present in the file.
        /// </summary>
        private void ValidateChunks()
        {
            if (!HasIffChunk("MOHD"))
                throw new Exception("Missing 'MOHD' chunk.");

            if (!HasIffChunk("MOTX"))
                throw new Exception("Missing 'MOTX' chunk.");

            if (!HasIffChunk("MOMT"))
                throw new Exception("Missing 'MOMT' chunk.");

            if (!HasIffChunk("MOGI"))
                throw new Exception("Missing 'MOGI' chunk.");

            if (!HasIffChunk("MODN"))
                throw new Exception("Missing 'MODN' chunk.");

            if (!HasIffChunk("MODD"))
                throw new Exception("Missing 'MODD' chunk.");
        }

        /// <summary>
        /// Checks if a certain IFF chunk is present in the WMO
        /// </summary>
        /// <param name="id">4-byte id of the chunk</param>
        /// <returns>true if the chunks is contained, false otherwise</returns>
        private bool HasIffChunk(string id)
        {
            if (id.Length != 4)
                return false;

            var bytes = Encoding.ASCII.GetBytes(id);
            var sig = (uint)((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3]);
            return mIffChunks.ContainsKey(sig);
        }

        /// <summary>
        /// Retrieves an IFF chunk by its 4-byte identifier
        /// </summary>
        /// <param name="id">4 byte identifier of the chunk</param>
        /// <returns>The chunk with the selected id</returns>
        private IffChunk GetIffChunk(string id)
        {
            if (id.Length != 4)
                throw new InvalidOperationException("Invalid signature for chunk");

            var bytes = Encoding.ASCII.GetBytes(id);
            var sig = (uint)((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3]);

            return mIffChunks[sig];
        }

        /// <summary>
        /// Gets the groups of the WMO file after it has been loaded.
        /// </summary>
        public List<WmoGroup> Groups => mGroups;

        /// <summary>
        /// Gets the materials from the WMO file.
        /// </summary>
        public List<MOMT> Materials => mMaterials;

        public List<MODS> DoodadSets => mDoodadSets;

        private readonly Dictionary<uint, IffChunk> mIffChunks = new Dictionary<uint, IffChunk>();
        private readonly System.IO.BinaryReader mReader;
        private MODS mSelectedSet;

        private List<MODD> mDoodadInstances = new List<MODD>();
        private readonly List<string> mMaterialTextures = new List<string>();
        private List<MOMT> mMaterials = new List<MOMT>();
        private readonly List<WmoGroup> mGroups = new List<WmoGroup>();
        private readonly List<Rendering.Texture> mTextures = new List<Rendering.Texture>();
        private readonly List<string> mModelNames = new List<string>();
        private List<MODS> mDoodadSets = new List<MODS>();

        private readonly string mRootPath;
        private readonly string mModelName;

        private MOHD mHeader;
    }
}
