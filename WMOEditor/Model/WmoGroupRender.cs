﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.Direct3D9;

namespace WMOEditor.Model
{
    public class WmoGroupRender
    {
        /// <summary>
        /// Constructs the group renderer for a WMO group.
        /// </summary>
        /// <param name="group">The wmo group to render</param>
        public WmoGroupRender(WmoGroup group)
        {
            mGroup = group;
        }

        /// <summary>
        /// Static constructor loads the vertex declaration
        /// </summary>
        static WmoGroupRender()
        {
            mDeclaration = new VertexDeclaration(Rendering.RenderLoop.Instance.Device.Device, mElements);

            mWmoProgram = new Rendering.ShaderProgram(
                new Rendering.ShaderCode(Encoding.ASCII.GetString(Shaders.WmoShader), "VertexMain", "vs_3_0"),
                new Rendering.ShaderCode(Encoding.ASCII.GetString(Shaders.WmoShader), "PixelMain", "ps_3_0"));
        }

        /// <summary>
        /// Unloads all unmanaged resources from the renderer.
        /// </summary>
        public void Unload()
        {
            mVertexBuffer.Dispose();
            mIndexBuffer.Dispose();
        }

        /// <summary>
        /// Loads all the data neccessary to render this wmo group
        /// </summary>
        public void Load()
        {
            var dev = Rendering.RenderLoop.Instance.Device.Device;

            mVertexBuffer = new VertexBuffer(dev, mGroup.Vertices.Count * WmoVertex.Size, Usage.None, VertexFormat.None, Pool.Managed);
            mIndexBuffer = new IndexBuffer(dev, mGroup.Indices.Count * 2, Usage.None, Pool.Managed, true);

            var strm = mVertexBuffer.Lock(0, 0, LockFlags.None);
            strm.WriteRange(mGroup.Vertices.ToArray());
            mVertexBuffer.Unlock();

            strm = mIndexBuffer.Lock(0, 0, LockFlags.None);
            strm.WriteRange(mGroup.Indices.ToArray());
            mIndexBuffer.Unlock();
        }

        /// <summary>
        /// Renders the geometry of the associated wmo group.
        /// </summary>
        public void Render()
        {
            var dev = Rendering.RenderLoop.Instance.Device.Device;

            dev.Indices = mIndexBuffer;
            dev.SetStreamSource(0, mVertexBuffer, 0, WmoVertex.Size);
            dev.VertexDeclaration = mDeclaration;

            mWmoProgram.Apply();
            mWmoProgram.Set("matView", Rendering.RenderLoop.Instance.Device.Camera.ViewMatrix);
            mWmoProgram.Set("matProj", Rendering.RenderLoop.Instance.Device.ProjectionMatrix);

            foreach (var batch in mGroup.Batches)
            {
                mGroup.ApplyMaterial(batch.material);
                dev.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, mGroup.Vertices.Count, (int)batch.startIndex, batch.numIndices / 3);
            }
        }

        private VertexBuffer mVertexBuffer;
        private IndexBuffer mIndexBuffer;

        private WmoGroup mGroup;

        private static Rendering.ShaderProgram mWmoProgram;
        private static VertexDeclaration mDeclaration;
        private static VertexElement[] mElements = new VertexElement[]
        {
            new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0),
            new VertexElement(0, 12, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.Color, 0),
            new VertexElement(0, 28, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
            new VertexElement(0, 36, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Normal, 0),
            VertexElement.VertexDeclarationEnd
        };
    }
}
